package com.putao.config;

import com.putao.interceptor.FeignRequestInterceptor;
import com.putao.interceptor.MyInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration      // 配置拦截器
public class MvcJavaConfig implements WebMvcConfigurer {

    @Autowired
    private MyInterceptor myInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(myInterceptor)
                .addPathPatterns("/**")                 // 拦截判断登录路径
                .excludePathPatterns("/open/**", "/swagger-resource/**", "/swagger-resources");           // 放行路径 (开放接口，接口文档)
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    //@Override
    //public void addViewControllers(ViewControllerRegistry registry) {
    //     //实现直接/ 是访问登陆 界面。注意login需要放在templates目录下，都则报404
    //    //registry.addViewController("/").setViewName("login");
    //    //registry.addViewController("/login.html").setViewName("login");
    //    registry.addViewController("/main.html").setViewName("menu/index");
    //}

    // 请求拦截
    @Bean
    public FeignRequestInterceptor feignRequestInterceptor(){
        return new FeignRequestInterceptor();
    }
}
