package com.putao.web.controller;


import com.putao.base.BaseController;
import com.putao.domain.SysOperateLog;
import com.putao.result.JsonResult;
import com.putao.service.SysOperateLogService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 操作日志表 前端控制器
 * </p>
 *
 * @author putao
 * @since 2022-02-23
 */
@RestController
@RequestMapping("/sysOperateLogs")
public class SysOperateLogController extends BaseController<SysOperateLog> {

    @Autowired
    private SysOperateLogService operateLogService;

    @PostMapping
    @ApiOperation(value = "新增操作记录日志", notes = "新增操作记录日志")
//    @OperateMethodName(value = "新增操作记录日志", method = "sysOperateLog:insert")
    public JsonResult insert(@RequestBody SysOperateLog sysOperateLog){
        boolean save = operateLogService.insertSysOperateLog(sysOperateLog);

        return save ? insertSuccess(sysOperateLog, null) : JsonResult.operationError(null, sysOperateLog, null);
    }
}


