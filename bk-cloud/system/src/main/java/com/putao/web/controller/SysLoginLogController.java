package com.putao.web.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 登录日志表 前端控制器
 * </p>
 *
 * @author putao
 * @since 2022-02-23
 */
@RestController
@RequestMapping("/sys-login-log")
public class SysLoginLogController {

}

