package com.putao.web.controller;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.putao.annotation.OperateMethodName;
import com.putao.base.BaseController;
import com.putao.base.BaseDomain;
import com.putao.domain.SysDict;
import com.putao.domain.SysDictItem;
import com.putao.result.JsonResult;
import com.putao.service.SysDictItemService;
import com.putao.service.SysDictService;
import com.putao.utils.StringUtils;
import com.putao.vo.DictItemVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 字典值具体的选项值表 前端控制器
 * </p>
 *
 * @author putao
 * @since 2022-01-13
 */
@RestController
@RequestMapping("/dictItems")
//@Api(tags = {"6.0 字典值增删改查类"})
@Slf4j
public class SysDictItemController extends BaseController<SysDictItem> {
    @Autowired
    private SysDictItemService dictItemService;
    @Autowired
    private SysDictService dictService;

    @GetMapping
    //@ApiOperation(value = "查询字典值列表")
    @OperateMethodName(value = "字典值列表", method = "dictItem:get")
    public JsonResult get(DictItemVO dictItemVO, HttpServletRequest request){
        PageHelper.startPage(dictItemVO.getPageIndex(), dictItemVO.getPageSize());
        List<SysDictItem> dictItemList = new ArrayList<>();
        QueryWrapper<SysDictItem> queryWrapper = createWrapperAndDelFlag();
        if (Strings.isNotBlank(dictItemVO.getName()) ){
            queryWrapper.like("item_text", dictItemVO.getName());
        }
        if(dictItemVO.getDictId() != null && "16".equals(dictItemVO.getDictId())){
            queryWrapper.eq("dict_id", dictItemVO.getDictId());
        }else{
            dictItemVO.setDictId("16");
        }
        dictItemList = dictItemService.list(queryWrapper);
        for (SysDictItem sysDictItem : dictItemList) {
            SysDict dict = dictService.getById(sysDictItem.getDictId());
            if (ObjectUtil.isNotEmpty(dict)){
                sysDictItem.setDictName(dict.getDictName());
            }
        }
        dictItemVO.setTotalNum(dictItemService.count(queryWrapper));
        return JsonResult.selectSuccess(dictItemList, dictItemVO);
    }

    @PostMapping
    //@ApiOperation(value = "插入", notes = "")
    @OperateMethodName(value = "字典值插入", method = "dictItem:insert")
    public JsonResult insert(@RequestBody SysDictItem dictItem){
        SysDictItem newDict = dictItemService.saveDictItem(dictItem);
        return insertSuccess(newDict, newDict);
//        if (dictItem == null) {
//            return JsonResult.insertArgumentError();
//        }
//        insertInit(dictItem);
//        boolean save = dictItemService.save(dictItem);
//        return save ? JsonResult.insertSuccess(null) : JsonResult.insertFail();
    }

    /**
     * 将对象的数据初始化，例如修改时间，创建时间
     * @param dictItem
     */
    private void insertInit(SysDictItem dictItem) {

    }

    @DeleteMapping
    //@ApiOperation(value = "删除", notes = "路径参数传入id")
    @OperateMethodName(value = "字典值删除", method = "dictItem:delete")
    public JsonResult delete(@PathVariable("id") String id){
        if (!Strings.isNotBlank(id)){
            return deleteNotFound(id, null);
        }
        boolean result = false;
        List<String> idList = StringUtils.convertList(id);
        if (CollectionUtil.isNotEmpty(idList)) {
            Collection<SysDictItem> dictItemsList = dictItemService.listByIds(idList);
            if ( CollectionUtil.isNotEmpty(dictItemsList)) {
                for (SysDictItem dictItem : dictItemsList) {
                    if (BaseDomain.DEL_FLAG_DELETE.equals(dictItem.getDelFlag())) {
                        return deleteErrorByStatus(id, null);
                    }
                    dictItem.setDelFlag(BaseDomain.DEL_FLAG_DELETE);
                }
                result = dictItemService.updateBatchById(dictItemsList);
            }
        }
        return result ? deleteSuccess(id, null) : deleteNotFound(id, null);
    }

    @PutMapping
    //@ApiOperation(value = "修改", notes = "路径参数传入id")
    @OperateMethodName(value = "字典值修改", method = "dictItem:update")
    public JsonResult update(@RequestBody SysDictItem dictItem){
        if (!Strings.isNotBlank(dictItem.getId().toString())){
            return updateNotFound(dictItem, null);
        }
        boolean result = dictItemService.update(dictItem, new QueryWrapper<SysDictItem>().eq("id", dictItem.getId()));
        return updateSuccess(dictItem, null);
    }

    @GetMapping("/selectByDictId")
    //@ApiOperation(value = "根据id查询字典值", notes = "路径参数传入id")
    @OperateMethodName(value = "根据id查询字典值", method = "dictItem:selectByDictId")
    public JsonResult selectByDictId(String dictId){
        if (StringUtils.isBlank(dictId)){
            return selectNotFound(dictId, null);
        }
        List<SysDictItem> sysDictItemList = dictItemService.list(createWrapperAndDelFlag().eq("dict_id", dictId));
        List<String> itemTextList = sysDictItemList.stream().map(SysDictItem::getItemText).collect(Collectors.toList());
        return selectSuccess(itemTextList, itemTextList.size());
    }
}

