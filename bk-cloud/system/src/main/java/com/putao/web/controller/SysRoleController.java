package com.putao.web.controller;


import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.putao.annotation.OperateMethodName;
import com.putao.base.BaseController;
import com.putao.base.BaseDomain;
import com.putao.domain.SysPermission;
import com.putao.domain.SysRole;
import com.putao.result.JsonResult;
import com.putao.service.SysPermissionService;
import com.putao.service.SysRoleService;
import com.putao.utils.StringUtils;
import com.putao.utils.ThreadLocalUtil;
import com.putao.vo.BaseVo;
import com.putao.vo.RoleVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 角色表
 前端控制器
 * </p>
 *
 * @author putao
 * @since 2022-02-10
 */
@RestController
@RequestMapping("/roles")
//@Api(tags = {"3.0 角色增删改查类"})
public class SysRoleController extends BaseController<SysRole> {
    @Autowired
    private SysRoleService roleService;
    @Autowired
    private SysPermissionService permissionService;

    /**
     * 角色列表
     *
     * @return
     */
    @GetMapping
//    @RequiresRoles(value={"admin"})
//    @ApiOperation(value = "角色列表", notes = "base对象")
    @OperateMethodName(value = "角色列表", method = "sysRole:list")
    public JsonResult loginOut(BaseVo baseVo) {
        PageHelper.startPage(baseVo.getPageIndex(), baseVo.getPageSize());
        QueryWrapper<SysRole> wrapper = createWrapper();
        wrapper.like(Strings.isNotBlank(baseVo.getSearch()), "name", baseVo.getSearch());
        wrapper.eq(StringUtils.isNotBlank(baseVo.getColumnName())
                && StringUtils.isNotBlank(baseVo.getSelect()), baseVo.getColumnName(), baseVo.getSelect());
        orderByModifyTimeDesc(wrapper);
        List<SysRole> list = roleService.list(wrapper);
        if (CollectionUtil.isNotEmpty(list)) {
            List<SysPermission> permissionList = permissionService.list(new QueryWrapper<SysPermission>().eq("del_flag", BaseDomain.DEL_FLAG_NORMAL));
            if (CollectionUtil.isNotEmpty(permissionList)) {
                for (SysRole sysRole : list) {
                    for (SysPermission sysPermission : permissionList) {
                        if (sysRole.getPermissionIds().contains(sysPermission.getId())) {
                            sysRole.getList().add(sysPermission);
                        }
                    }
                }
            }
            return selectSuccess(list, roleService.count(wrapper));
        }
        return selectNotFound(new ArrayList<>(), 0);
    }

    @PutMapping
    //@ApiOperation(value = "角色修改", notes = "路径参数传入角色对象")
    @OperateMethodName(value = "角色修改", method = "sysRole:update")
    public JsonResult update(@RequestBody SysRole sysRole){
        if (!Strings.isNotBlank(sysRole.getId())){
            return updateNotFound(sysRole, null);
        }
        sysRole.setModifyTime(new Date());
        sysRole.setModifyId(ThreadLocalUtil.getUserId());
        boolean result = roleService.updateById(sysRole);
        return updateSuccess(sysRole, null);
    }


    @PostMapping
    //@ApiOperation(value = "添加角色", notes = "")
    @OperateMethodName(value = "添加角色", method = "sysRole:insert")
    public JsonResult insert(@RequestBody RoleVO roleVO){
        SysRole sysRole = roleService.saveRole(roleVO);
        return insertSuccess(sysRole, roleVO);
    }

    @DeleteMapping
    //@ApiOperation(value = "删除", notes = "参数传入id")
    @OperateMethodName(value = "角色删除,批量", method = "sysRole:delete")
    public JsonResult delete(String id){
        if (!Strings.isNotBlank(id)){
            return argumentError(id, null);
        }
        List<String> idList = StringUtils.convertList(id);
        boolean result = false;
        if (CollectionUtil.isNotEmpty(idList)) {
            Collection<SysRole> sysRoleList = roleService.listByIds(idList);
            if (CollectionUtil.isNotEmpty(sysRoleList)) {
                for (SysRole sysRole : sysRoleList) {
                    if (BaseDomain.DEL_FLAG_DELETE.equals(sysRole.getDelFlag())) {
                        return deleteErrorByStatus(id, null);
                    }
                    sysRole.setDelFlag(BaseDomain.DEL_FLAG_DELETE);
                }
                result = roleService.updateBatchById(sysRoleList);
            }
        }
        return result ? deleteSuccess(id, null) : deleteNotFound(id, null);
    }
}

