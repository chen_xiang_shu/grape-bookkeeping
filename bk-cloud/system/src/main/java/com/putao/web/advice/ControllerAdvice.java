package com.putao.web.advice;

import com.putao.error.BusinessException;
import com.putao.result.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/*
 @Author:putao
 @Date:2022-06-26 14:13:14
*/
@RestControllerAdvice
@Slf4j
public class ControllerAdvice {

    @ExceptionHandler(value = Exception.class)
    public JsonResult handler(Exception ex){
        if (ex instanceof MethodArgumentNotValidException){
            log.error("参数错误：{}", ex.getMessage());
            return JsonResult.<Void>argumentError(ex.getMessage(), null);
        }
        ex.printStackTrace();
        log.error("系统出现了异常：{}", ex.getMessage());
        return JsonResult.<Void>systemError(ex.getMessage(), null);
    }

    @ExceptionHandler(value = BusinessException.class)
    public JsonResult handler(BusinessException ex){
        ex.printStackTrace();
        log.error("认证出现错误：{}", ex.getMessage());
        return JsonResult.<Void>operationError(ex.getMessage(), ex.getMessage(), ex.getMessage());
    }

}
