package com.putao.web.controller;


import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.putao.annotation.OperateMethodName;
import com.putao.base.BaseController;
import com.putao.base.BaseDomain;
import com.putao.domain.SysPermission;
import com.putao.result.JsonResult;
import com.putao.service.SysPermissionService;
import com.putao.utils.StringUtils;
import com.putao.utils.ThreadLocalUtil;
import com.putao.vo.BaseVo;
import com.putao.vo.PermissionVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author putao
 * @since 2022-02-10
 */
@RestController
@RequestMapping("/permissions")
//@Api(tags = {"4.0 权限增删改查类"})
public class SysPermissionController extends BaseController<SysPermission> {

    @Autowired
    private SysPermissionService permissionService;

    @GetMapping
    //@ApiOperation(value = "权限列表", notes = "base对象")
    @OperateMethodName(value = "权限列表", method = "sysPermission:list")
    public JsonResult loginOut(BaseVo baseVo) {
        PageHelper.startPage(baseVo.getPageIndex(), baseVo.getPageSize());
        QueryWrapper<SysPermission> wrapper = createWrapper();
        wrapper.like(Strings.isNotBlank(baseVo.getSearch()), "name", baseVo.getSearch());
        wrapper.eq(StringUtils.isNotBlank(baseVo.getColumnName())
                && StringUtils.isNotBlank(baseVo.getSelect()), baseVo.getColumnName(), baseVo.getSelect());
        orderByModifyTimeDesc(wrapper);
        List<SysPermission> list = permissionService.list(wrapper);
        if (CollectionUtil.isNotEmpty(list)) {
            return selectSuccess(list, permissionService.count(wrapper));
        }
        return selectNotFound(new ArrayList<>(), 0);
    }

    @PutMapping
    //@ApiOperation(value = "权限修改", notes = "路径参数传入权限对象")
    @OperateMethodName(value = "权限修改", method = "sysPermission:update")
    public JsonResult update(@RequestBody SysPermission sysPermission){
        if (!Strings.isNotBlank(sysPermission.getId())){
            return updateNotFound(sysPermission, null);
        }
        sysPermission.setModifyTime(new Date());
        sysPermission.setModifyId(ThreadLocalUtil.getUserId());
        boolean result = permissionService.updateById(sysPermission);
        return updateSuccess(sysPermission, result);
    }


    @PostMapping
    //@ApiOperation(value = "添加权限", notes = "")
    @OperateMethodName(value = "添加权限", method = "sysPermission:insert")
    public JsonResult insert(@RequestBody PermissionVO permissionVO){
        SysPermission sysPermission = permissionService.savePermission(permissionVO);
        return insertSuccess(sysPermission, permissionVO);
    }

    @DeleteMapping
    //@ApiOperation(value = "删除", notes = "参数传入id")
    @OperateMethodName(value = "权限删除,批量", method = "sysPermission:delete")
    public JsonResult delete(String id){
        if (!Strings.isNotBlank(id)){
            return argumentError(id, null);
        }
        List<String> idList = StringUtils.convertList(id);
        boolean result = false;
        if (CollectionUtil.isNotEmpty(idList)) {
            Collection<SysPermission> sysPermissionList = permissionService.listByIds(idList);
            if (CollectionUtil.isNotEmpty(sysPermissionList)) {
                for (SysPermission sysPermission : sysPermissionList) {
                    if (BaseDomain.DEL_FLAG_DELETE.equals(sysPermission.getDelFlag())) {
                        return deleteErrorByStatus(id, null);
                    }
                    sysPermission.setDelFlag(BaseDomain.DEL_FLAG_DELETE);
                }
                result = permissionService.updateBatchById(sysPermissionList);
            }
        }
        return result ? deleteSuccess(id, null) : deleteNotFound(id, null);
    }
}

