package com.putao.web.controller;


import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.putao.annotation.OperateMethodName;
import com.putao.base.BaseController;
import com.putao.base.BaseDomain;
import com.putao.domain.SysDict;
import com.putao.result.JsonResult;
import com.putao.service.SysDictService;
import com.putao.utils.StringUtils;
import com.putao.vo.BaseVo;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 字典类型表，例如状态，状态有两种值，正常 和 冻结 前端控制器
 * </p>
 *
 * @author putao
 * @since 2022-02-09
 */
@RestController
//@Api(tags = {"5.0 字典增删改查类"})
@RequestMapping("/dicts")
public class SysDictController extends BaseController<SysDict> {

    @Autowired
    private SysDictService dictService;

    @GetMapping
    //@ApiOperation(value = "查询字典列表")
//    @RequiresPermissions("dict:get")
    @OperateMethodName(value = "字典类型列表", method = "dict:get")
    public JsonResult get(BaseVo baseVo){
        PageHelper.startPage(baseVo.getPageIndex(), baseVo.getPageSize());
        QueryWrapper<SysDict> wrapper = new QueryWrapper<>();
        wrapper.like(Strings.isNotBlank(baseVo.getSearch()), "dict_name", baseVo.getSearch());
        wrapper.eq(StringUtils.isNotBlank(baseVo.getColumnName())
                && StringUtils.isNotBlank(baseVo.getSelect()), baseVo.getColumnName(), baseVo.getSelect());
        orderByModifyTimeDesc(wrapper);
        List<SysDict> list = dictService.list(wrapper);
        if (CollectionUtil.isNotEmpty(list)) {
            return selectSuccess(list, dictService.count(wrapper));
        }
        return JsonResult.selectSuccess(list, dictService.count(wrapper));
    }

    @PostMapping
    //@ApiOperation(value = "插入", notes = "")
    @OperateMethodName(value = "字典类型插入", method = "dict:insert")
    public JsonResult insert(@RequestBody SysDict dict){
        SysDict newDict = dictService.saveDict(dict);
        return insertSuccess(newDict, newDict);
    }

    @DeleteMapping
    //@ApiOperation(value = "删除", notes = "路径参数传入id")
    @OperateMethodName(value = "字典类型删除", method = "dict:delete")
    public JsonResult delete(String id){
        if (!Strings.isNotBlank(id)){
            return deleteNotFound(id, null);
        }
        List<String> idList = StringUtils.convertList(id);
        boolean result = false;
        if (CollectionUtil.isNotEmpty(idList)) {
            Collection<SysDict> dictList = dictService.listByIds(idList);
            if (CollectionUtil.isNotEmpty(dictList)) {
                for (SysDict sysDict : dictList) {
                    if (BaseDomain.DEL_FLAG_DELETE.equals(sysDict.getDelFlag())) {
                        return deleteErrorByStatus(id, null);
                    }
                    sysDict.setDelFlag(BaseDomain.DEL_FLAG_DELETE);
                }
                result = dictService.updateBatchById(dictList);
            }
        }
        return result ? deleteSuccess(id, null) : deleteNotFound(id, null);
    }

    @PutMapping
    //@ApiOperation(value = "修改", notes = "路径参数传入id")
    @OperateMethodName(value = "字典类型修改", method = "dict:update")
    public JsonResult update(@RequestBody SysDict dict){
        if (!Strings.isNotBlank(dict.getId().toString())){
            return updateNotFound(dict, null);
        }
        dictService.updateById(dict);
        return  updateSuccess(dict, null);
    }
}

