package com.putao.web.controller;


import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.putao.annotation.OperateMethodName;
import com.putao.base.BaseController;
import com.putao.base.BaseDomain;
import com.putao.domain.SysMenu;
import com.putao.result.JsonResult;
import com.putao.service.SysMenuService;
import com.putao.utils.MenuUtils;
import com.putao.utils.StringUtils;
import com.putao.utils.ThreadLocalUtil;
import com.putao.vo.BaseVo;
import com.putao.vo.MenuVo;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 菜单表
 前端控制器
 * </p>
 *
 * @author putao
 * @since 2022-02-10
 */
@RestController
@RequestMapping("/menus")
//@Api(tags = {"8.0 菜单增删改查类"})
public class SysMenuController extends BaseController<SysMenu> {
    @Autowired
    private SysMenuService menuService;

    @GetMapping
    //@ApiOperation(value = "查询菜单列表")
    @OperateMethodName(value = "菜单列表", method = "menu:get")
    public JsonResult get(BaseVo baseVo, String userId){
        PageHelper.startPage(baseVo.getPageIndex(), baseVo.getPageSize());
        QueryWrapper<SysMenu> queryWrapper = createWrapper();
        List<SysMenu> menuList = menuService.list(queryWrapper.eq("modify_id", userId));
        if (CollectionUtil.isNotEmpty(menuList)) {
            List<MenuVo> menuVoList = MenuUtils.recursionSysMenu("0", menuList);
            if (CollectionUtil.isNotEmpty(menuVoList)) {
                return selectSuccess(menuVoList, menuService.count(queryWrapper));
            }
        }
        return selectNotFound(baseVo, null);
    }

    @PutMapping
    //@ApiOperation(value = "用戶修改", notes = "路径参数传入用戶对象")
    @OperateMethodName(value = "用戶修改", method = "sysMenu:update")
    public JsonResult update(@RequestBody SysMenu sysMenu){
        if (!Strings.isNotBlank(sysMenu.getId())){
            return updateNotFound(sysMenu, null);
        }
        sysMenu.setModifyTime(new Date());
        sysMenu.setModifyId(ThreadLocalUtil.getUserId());
        boolean result = menuService.updateById(sysMenu);
        return updateSuccess(sysMenu, null);
    }


    @PostMapping
    //@ApiOperation(value = "添加用戶", notes = "")
    @OperateMethodName(value = "添加用戶", method = "sysMenu:insert")
    public JsonResult insert(@RequestBody MenuVo menuVO){
        SysMenu sysMenu = menuService.saveMenu(menuVO);
        return insertSuccess(sysMenu, menuVO);
    }

    @DeleteMapping
    //@ApiOperation(value = "删除", notes = "参数传入id")
    @OperateMethodName(value = "用户删除,批量", method = "sysMenu:delete")
    public JsonResult delete(String id){
        if (!Strings.isNotBlank(id)){
            return argumentError(id, null);
        }
        List<String> idList = StringUtils.convertList(id);
        boolean result = false;
        if (CollectionUtil.isNotEmpty(idList)) {
            Collection<SysMenu> sysMenuList = menuService.listByIds(idList);
            if (CollectionUtil.isNotEmpty(sysMenuList)) {
                for (SysMenu sysMenu : sysMenuList) {
                    if (BaseDomain.DEL_FLAG_DELETE.equals(sysMenu.getDelFlag())) {
                        return deleteErrorByStatus(id, null);
                    }
                    sysMenu.setDelFlag(BaseDomain.DEL_FLAG_DELETE);
                }
                result = menuService.updateBatchById(sysMenuList);
            }
        }
        return result ? deleteSuccess(id, null) : deleteNotFound(id, null);
    }
}

