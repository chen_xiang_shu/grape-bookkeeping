//package com.putao.web.controller;
//
//import cn.hutool.core.util.ObjectUtil;
//import com.alibaba.nacos.common.utils.UuidUtils;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.putao.annotation.OperateMethodName;
//import com.putao.constants.Constants;
//import com.putao.domain.SysUser;
//import com.putao.enums.SystemCodeEnum;
//import com.putao.enums.VerifyCodeEnum;
//import com.putao.redis.service.IRedisService;
//import com.putao.result.JsonResult;
//import com.putao.service.SysUserService;
//import com.putao.utils.ReqUtils;
//import com.putao.utils.StringUtils;
//import com.putao.vo.LoginVo;
//import com.putao.vo.UserVO;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 类描述： 开放接口类
// *
// * @author fuhehuang
// * @version 1.0
// * @address
// * @email fhehuang@163.com
// * @date 2022/4/28 9:27
// */
////@Api(tags = {"1.0 开放类"})
//@RestController
//@RequestMapping("open")
//@Slf4j
//public class SysOpenController {
//
//    @Value("${annourl.url}")
//    private String url;
//
//    @Autowired
//    private SysUserService userService;
//    @Autowired
//    private IRedisService redisService;
//
//
//    @GetMapping("a")
//    public Map<String, Object> callback1(){
//        Map<String, Object> result = new HashMap<>();
//        System.out.println(url);
//        result.put("errcode", 0);
//        result.put("message", "成功");
//        return result;
//    }
//
//    @GetMapping("test")
//    public Object test(){
//        return ReqUtils.req("https://server01.vicy.cn/8lXdSX7FSMykbl9nFDWESdc6zfouSAEz/wxLogin/tempUserId?secretKey=dab21aad836a4b8fb289f08458366515", Constants.REQUEST_TYPE_GET);
//    }
//}
