package com.putao.interceptor;

import com.putao.utils.ThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Component
@Slf4j
public class MyInterceptor implements HandlerInterceptor, CommonInterceptor {

    // 前置拦截
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return this.preHandle(request, response, handler, log);
    }

    // 后置拦截
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    // 最后拦截
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // 1、从请求头中获取uuid
        // 2、根据uuid组成key到redis中重置value的时间
        //log.error("----------  删除前用户是： " + ThreadLocalUtil.get());
        ThreadLocalUtil.remove();
        //log.error("----------     删除完成     ----------");
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
