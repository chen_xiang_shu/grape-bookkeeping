package com.putao.feign.callback;

import com.putao.feign.WebsocketFeign;
import com.putao.result.JsonResult;
import com.putao.result.WebsocketResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class WebsocketFeignCallback implements WebsocketFeign {

    @Override
    public JsonResult sendMessage(WebsocketResult message) {
        log.error("Websocket Server Fallback..........");
        return JsonResult.callback("兜底数据", null);
    }
}
