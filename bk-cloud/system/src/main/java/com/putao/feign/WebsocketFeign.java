package com.putao.feign;

import com.putao.feign.callback.WebsocketFeignCallback;
import com.putao.result.JsonResult;
import com.putao.result.WebsocketResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/*
 @Author:putao
 @Date:2022-07-11 15:05:35
*/
/**
 * user的feign接口
 */
@FeignClient(name = "websocket-service", fallback = WebsocketFeignCallback.class)
public interface WebsocketFeign {

    @PostMapping("/messages")
    JsonResult sendMessage(@RequestBody WebsocketResult message);

}
