
package com.putao.rocketmq.listener;

import com.putao.enums.VerifyCodeEnum;
import com.putao.feign.WebsocketFeign;
import com.putao.redis.service.IRedisService;
import com.putao.result.WebsocketResult;
import com.putao.utils.StringUtils;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;


/*
 @Author:putao
 @Date:2022-07-22 21:56:54
*/
@Component
@RocketMQMessageListener(consumerGroup = "consumer-order",topic = "topic-a")
public class UserListener implements RocketMQListener<String> {

    @Autowired
    private IRedisService redisService;

    @Autowired
    private WebsocketFeign websocketFeign;


    private final Logger logger = LoggerFactory.getLogger(UserListener.class);

    @Override
    public void onMessage(String uuid) {
        // 判断uuid是否存在不存在则不需要操作，反之表示还未登录则发送消息给前端，二维码过期
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss").format(new Date());
        logger.error("当前时间是 " + format);
        logger.error("收到消息并开始处理");
        String value = redisService.get(VerifyCodeEnum.QR_CODE_TIME_OUT.join(uuid));
        if (StringUtils.isNotBlank(value)) {
            logger.error("设置key：{}过期，发送消息给pc", VerifyCodeEnum.QR_CODE_TIME_OUT.join(uuid));
            // 存在表示到时了用户尚未扫码后者扫码后尚未授权，这时将二维码改为失效，删除即可或者重置时间为0
            redisService.deleteKey(VerifyCodeEnum.QR_CODE_TIME_OUT.join(uuid));
            // 发送消息给前端
            websocketFeign.sendMessage(WebsocketResult.timeOut(null, uuid));
        } else {
            logger.error("key：{} 不存在，无法执行下面流程", VerifyCodeEnum.QR_CODE_TIME_OUT.join(uuid));
        }
    }

}
