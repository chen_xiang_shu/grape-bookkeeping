package com.putao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.putao.base.BaseService;
import com.putao.domain.SysDict;
import com.putao.mapper.SysDictMapper;
import com.putao.service.SysDictService;
import com.putao.utils.AssertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 * 字典类型表，例如状态，状态有两种值，正常 和 冻结 服务实现类
 * </p>
 *
 * @author putao
 * @since 2022-02-09
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService, BaseService<SysDict> {

    @Autowired
    private SysDictMapper sysDictMapper;

    @Override
    public Integer selectMaxDictCode() {
        return sysDictMapper.selectMaxDictCode();
    }

    @Override
    public SysDict saveDict(SysDict dict) {
        AssertUtil.hasText(dict.getDictName(), "字典名称不能为空");
        AssertUtil.hasText(dict.getType(), "字典类型不能为空");
        this.checkIsEmptyAndRepeat(null, new QueryWrapper<>(), dict, "dict_name", "字典名称重复了", "字典名称不能为空");
        this.checkIsEmptyAndRepeat(null, new QueryWrapper<>(), dict, "type", "字典类型重复了", "字典类型不能为空");
        dict.insertInit();
        Integer count = this.selectMaxDictCode();
        if(ObjectUtils.isEmpty(count)) {
            count = 1001;
        }
        dict.setDictCode(count);
        this.save(dict);
        return dict;
    }
}
