package com.putao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.SysRole;
import com.putao.result.JsonResult;
import com.putao.vo.RoleVO;

public interface SysRoleService extends IService<SysRole> {

    /**
     * 根据权限ids查出所有的权限对象
     * @param roleIds
     * @return
     */
    JsonResult getRolesByRoleIds(String roleIds);

    SysRole saveRole(RoleVO roleVO);
}
