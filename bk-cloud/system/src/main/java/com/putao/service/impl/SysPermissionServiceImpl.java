package com.putao.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.putao.base.BaseService;
import com.putao.domain.SysPermission;
import com.putao.mapper.SysPermissionMapper;
import com.putao.mapper.SysRoleMapper;
import com.putao.service.SysPermissionService;
import com.putao.vo.PermissionVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements SysPermissionService, BaseService<SysPermission> {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    //@Override
    //public List<SysRolePermissionVo> listRolePermission() {
    //    //获取所有的权限集合
    //    List<SysPermission> permissions = sysPermissionMapper.list();
    //    //获取所有的权限对应的角色
    //    List<SysRolePermissionVo> list=new ArrayList<>();
    //    permissions.parallelStream().peek(p->{
    //        List<SysRole> roles = sysRoleMapper.listByPermissionId(p.getId());
    //        SysRolePermissionVo vo = new SysRolePermissionVo();
    //        BeanUtil.copyProperties(p,vo);
    //        vo.setRoles(roles);
    //        list.add(vo);
    //    }).collect(Collectors.toList());
    //    return list;
    //}

    @Override
    public SysPermission savePermission(PermissionVO permissionVO) {
        SysPermission permission = new SysPermission();
        BeanUtils.copyProperties(permissionVO, permission);
        this.checkIsEmptyAndRepeat(null, new QueryWrapper<>(), permission, "name", "权限名称重复了", "权限名称不能为空");
        this.checkIsEmptyAndRepeat(null, new QueryWrapper<>(), permission, "expression", "权限表达式重复了", "权限表达式不能为空");
        permission.insertInit();
        this.save(permission);
        return permission;
    }
}
