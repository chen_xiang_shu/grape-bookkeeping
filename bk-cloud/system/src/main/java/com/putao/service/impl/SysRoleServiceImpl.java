package com.putao.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.putao.base.BaseService;
import com.putao.domain.SysRole;
import com.putao.mapper.SysRoleMapper;
import com.putao.result.JsonResult;
import com.putao.service.SysRoleService;
import com.putao.utils.StringUtils;
import com.putao.vo.RoleVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService, BaseService<SysRole> {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public JsonResult getRolesByRoleIds(String roleIds) {
        List<SysRole> roleList = this.list(new QueryWrapper<SysRole>().in("id", StringUtils.convertList(roleIds)));
        return returnObject(roleList, roleIds);
    }

    @Override
    public SysRole saveRole(RoleVO roleVO) {
        SysRole role = new SysRole();
        BeanUtils.copyProperties(roleVO, role);
        this.checkIsEmptyAndRepeat(null, new QueryWrapper<>(), role, "name", "角色名称重复了", "角色名称不能为空");
        this.checkIsEmptyAndRepeat(null, new QueryWrapper<>(), role, "sn", "角色简称重复了", "角色简称不能为空");
        role.insertInit();
        this.save(role);
        return role;
    }
}
