package com.putao.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.putao.domain.SysOperateLog;
import com.putao.mapper.SysOperateLogMapper;
import com.putao.service.SysOperateLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author putao
 * @since 2022-02-23
 */
@Service
public class SysOperateLogServiceImpl extends ServiceImpl<SysOperateLogMapper, SysOperateLog> implements SysOperateLogService {

    @Override
    public boolean insertSysOperateLog(SysOperateLog sysOperateLog) {
        sysOperateLog.insertInit();
        return this.save(sysOperateLog);
    }
}
