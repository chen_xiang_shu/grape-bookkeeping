package com.putao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.SysOperateLog;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author putao
 * @since 2022-02-23
 */
public interface SysOperateLogService extends IService<SysOperateLog> {

    /**
     * 新增操作记录
     * @param sysOperateLog
     * @return
     */
    boolean insertSysOperateLog(SysOperateLog sysOperateLog);
}
