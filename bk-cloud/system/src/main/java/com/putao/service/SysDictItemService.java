package com.putao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.SysDictItem;

/**
 * <p>
 * 字典具体的选项值表 服务类
 * </p>
 *
 * @author putao
 * @since 2022-02-09
 */
public interface SysDictItemService extends IService<SysDictItem> {

    /**
     * 新增
     * @param dictItem
     * @return
     */
    SysDictItem saveDictItem(SysDictItem dictItem);
}
