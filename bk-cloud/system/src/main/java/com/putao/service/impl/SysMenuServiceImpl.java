package com.putao.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.putao.domain.SysMenu;
import com.putao.mapper.SysMenuMapper;
import com.putao.service.SysMenuService;
import com.putao.vo.MenuVo;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author putao
 * @since 2022-05-07
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    @Override
    public SysMenu saveMenu(MenuVo menuVO) {
        return null;
    }
}
