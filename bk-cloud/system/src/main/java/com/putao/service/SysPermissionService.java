package com.putao.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.SysPermission;
import com.putao.vo.PermissionVO;

public interface SysPermissionService extends IService<SysPermission> {

    SysPermission savePermission(PermissionVO permissionVO);
}
