package com.putao.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.SysUser;
import com.putao.result.JsonResult;
import com.putao.vo.UserVO;

public interface SysUserService extends IService<SysUser> {


    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    JsonResult getByUsername(String username);

    /**
     * 注册用户
     * @param userVO
     * @return
     */
    JsonResult register(UserVO userVO);

    /**
     * 删除旧二维码对应的标识并生成新的base64二维码返回
     * @param oldUuid 旧的标识
     * @param uuid 新的
     * @return
     */
    String getQrcode(String oldUuid, String uuid);

    /**
     * 根据手机号码查询用户信息
     * @param phone
     * @return
     */
    JsonResult getByPhone(String phone);
}
