package com.putao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.SysDict;

/**
 * <p>
 * 字典类型表，例如状态，状态有两种值，正常 和 冻结 服务类
 * </p>
 *
 * @author putao
 * @since 2022-02-09
 */
public interface SysDictService extends IService<SysDict> {

    Integer selectMaxDictCode();

    /**
     * 插入字典类型值
     * @param dict
     * @return
     */
    SysDict saveDict(SysDict dict);
}
