package com.putao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.SysMenu;
import com.putao.vo.MenuVo;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author putao
 * @since 2022-05-07
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 新增菜单
     * @param menuVO
     * @return
     */
    SysMenu saveMenu(MenuVo menuVO);
}
