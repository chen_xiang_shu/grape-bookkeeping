package com.putao.enums;

import lombok.Getter;

/*
 @Author:putao
 @Date:2022-07-22 22:03:36
*/
public enum SystemEnums {

    QR_CODE("topic-a", 10000, 5);

    @Getter
    private String destination;
    @Getter
    private int timeout;
    // 延时等级1到16分别表示 1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h,3表示延迟10s发送
    @Getter
    private int delayLevel;

    SystemEnums(String destination, int timeout, int delayLevel) {
        this.destination = destination;
        this.timeout = timeout;
        this.delayLevel = delayLevel;
    }
}
