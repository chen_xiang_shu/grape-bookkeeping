package com.putao.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.SysPermission;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description 权限表
 */
@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission> {
}
