package com.putao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.SysDict;

/**
 * <p>
 * 字典类型表，例如状态，状态有两种值，正常 和 冻结 Mapper 接口
 * </p>
 *
 * @author putao
 * @since 2022-01-13
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    Integer selectMaxDictCode();

}
