package com.putao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.SysDictItem;

/**
 * <p>
 * 字典具体的选项值表 Mapper 接口
 * </p>
 *
 * @author putao
 * @since 2022-02-09
 */
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
