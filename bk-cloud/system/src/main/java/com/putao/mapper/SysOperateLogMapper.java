package com.putao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.SysOperateLog;

/**
 * <p>
 * 操作日志表 Mapper 接口
 * </p>
 *
 * @author putao
 * @since 2022-02-23
 */
public interface SysOperateLogMapper extends BaseMapper<SysOperateLog> {

}
