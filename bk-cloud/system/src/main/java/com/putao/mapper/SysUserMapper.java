package com.putao.mapper;



import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.SysUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
}
