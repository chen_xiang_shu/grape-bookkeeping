package com.putao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.SysMenu;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author putao
 * @since 2022-05-07
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
