package com.putao.generate;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class SggCodeGenerator {

    public static void main(String[] args) {
//        String[] tableNames = {"sys_department", "sys_dict", "sys_dict_item", "sys_doctor"
//                , "sys_medicine", "sys_nurse", "sys_patient", "sys_ward"};
        String windowPath = "E:\\putao\\ideaproject\\bk-cloud\\common\\src\\main\\java";
//        String macPath = "/Users/putao/IdeaProjects/bk-cloud/common/src/main/java";
//        String[] tableNames = {"sys_user", "sys_charter_detail", "sys_charter_order", "sys_charter_merchant"};
        String[] tableNames = {"t_bill_record"};
        generate(tableNames, windowPath);
    }

    public static void generate(String[] tableNames, String path) {
        for (String tableName : tableNames) {


            // 1、创建代码生成器
            AutoGenerator mpg = new AutoGenerator();

            // 2、全局配置
            GlobalConfig gc = new GlobalConfig();
//        String projectPath = System.getProperty("user.dir");
            gc.setOutputDir(path);
            gc.setAuthor("putao");
            gc.setOpen(false); //生成后是否打开资源管理器
            gc.setFileOverride(false); //重新生成时文件是否覆盖
            gc.setServiceName("%sService"); //去掉Service接口的首字母I
            //if (tableName.startsWith("t")) {
            //    gc.setEntityName(gc.getEntityName().replaceAll("T", ""));
            //    gc.setMapperName(gc.getMapperName().replaceAll("T", ""));
            //    gc.setControllerName(gc.getControllerName().replaceAll("T", ""));
            //    gc.setServiceName(gc.getServiceName().replaceAll("T", ""));
            //}
            gc.setIdType(IdType.ID_WORKER_STR); //主键策略
            gc.setDateType(DateType.ONLY_DATE);//定义生成的实体类中日期类型
            gc.setSwagger2(false);//开启Swagger2模式

            mpg.setGlobalConfig(gc);

            // 3、数据源配置
            DataSourceConfig dsc = new DataSourceConfig();
            dsc.setUrl("jdbc:mysql://106.52.152.221:3306/bk-business?serverTimezone=GMT%2B8");
            dsc.setDriverName("com.mysql.cj.jdbc.Driver");
            dsc.setUsername("root");
            dsc.setPassword("root@1106");
            dsc.setDbType(DbType.MYSQL);
            mpg.setDataSource(dsc);

            // 4、包配置
            PackageConfig pc = new PackageConfig();
            pc.setModuleName(null); //模块名
            pc.setParent("com.putao");
//        pc.setController("web.controller");
            pc.setEntity("domain");
            pc.setService("service");
            pc.setMapper("mapper");
            mpg.setPackageInfo(pc);

            // 5、策略配置
            StrategyConfig strategy = new StrategyConfig();
            strategy.setInclude(tableName);//对那一张表生成代码
            strategy.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略
            strategy.setTablePrefix(pc.getModuleName() + "_"); //生成实体时去掉表前缀

            strategy.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略
            strategy.setEntityLombokModel(true); // lombok 模型 @Accessors(chain = true) setter链式操作

            strategy.setRestControllerStyle(true); //restful api风格控制器
            strategy.setControllerMappingHyphenStyle(true); //url中驼峰转连字符

            mpg.setStrategy(strategy);

            // 6、执行
            mpg.execute();
        }
    }
}
