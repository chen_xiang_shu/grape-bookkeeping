package com.putao.test;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public class QrTest {
    public static void main(String[] args) {
        QrCodeUtil.generate(//
                "http://hutool.cn/", //二维码内容
                QrConfig.create().setImg("e:/logo_small.jpg"), //附带logo
                null);
    }
}
