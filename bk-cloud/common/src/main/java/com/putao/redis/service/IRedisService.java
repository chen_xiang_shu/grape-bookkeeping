package com.putao.redis.service;

import java.util.concurrent.TimeUnit;


/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface IRedisService {

    /**
     * 存入redis
     * @param phone
     * @param verifyCode
     * @param minute
     * @param timeType
     */
    void set(String phone, String verifyCode, Long minute, TimeUnit timeType);

    /**
     * 存入redis并设置时间，时间类型：秒
     * @param key
     * @param value
     * @param seconds
     */
    void setAndTime(String key, String value, Long seconds);

    /**
     * 根据key获取到对应的value值
     * @param phone
     * @return
     */
    String getVerifyCode(String phone);

    /**
     * 登录后将token和userInfo对象 key ,value形式存入到redis中，并设置为30分钟
     * @param token 拼接好前缀
     * @param userInfo
     * @param userInfoTokenVaiTime
     */
    void setLoginToken(String token, String userInfo, int userInfoTokenVaiTime);

    /**
     * 登录后将uuid key ,jwt value形式存入到redis中，并设置为60分钟
     * @param uuid 拼接好前缀
     * @param jwt
     * @param userInfoTokenVaiTime
     */
    void setJwtByUUID(String uuid, String jwt, int userInfoTokenVaiTime);

    /**
     * 根据key重置value时间
     * @param joinedKey
     * @param time 单位 / 秒
     */
    void expire(String joinedKey, int time);

    /**
     * 创建token，并且使用UUID拼接起来
     * @param UUIDString
     * @return
     */
    String createToken(String UUIDString);

    /**
     * 根据前端传来的token（UUID）然后到redis中中获取到对应的value值
     * @param token2
     * @return
     */
    String getUserInfoByToken(String token2);

    /**
     * 往redis存入数据，value是json格式
     * @param joinedKey
     * @param value
     */
    void set(String joinedKey, String value);

    /**
     * 根据key查出对应的value
     * @param key
     * @return
     */
    String get(String key);


    /**
     * 根据key查出对应的value,实现类拼接
     * @return
     */
    String getByKey(String key);


    /**
     * 判断key是否存在
     * @param key
     * @return true:存在 false:不存在
     */
    boolean hasKey(String key);

    /**
     * 重置时间
     * @param token
     */
    void expireTime(String token);

    /**
     * 获取剩余时间
     * @param token
     * @return
     */
    Long getTime(String token);


        /**
         * 存入值并设置超时时间
         * @param key
         * @param minute 时间单位为分钟
         */
    void setValueAndTime(String key, String value, Long minute);

    /**
     * 根据拼接好的key获取value
     * @param key
     */
    Object getSplicedKey(String key);

    /**
     * 根据指定Key删除value
     * @param key
     * @return
     */
    Boolean deleteKey(String key);
}
