package com.putao.redis.service.impl;

import com.putao.constants.Consts;
import com.putao.enums.VerifyCodeEnum;
import com.putao.redis.service.IRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Component
public class RedisServiceImpl implements IRedisService {

    @Autowired
    private StringRedisTemplate template;

    @Override
    public void set(String phone, String verifyCode, Long minute, TimeUnit timeType) {
        template.opsForValue().set(phone, verifyCode, minute, timeType);
    }

    @Override
    public void setAndTime(String key, String value, Long seconds) {
        template.opsForValue().set(key, value, seconds, TimeUnit.SECONDS);
    }

    @Override
    public String getVerifyCode(String phone) {
        return template.opsForValue().get(phone);
    }

    @Override
    public void setLoginToken(String token, String userInfo, int userInfoTokenVaiTime) {
        template.opsForValue().set(token, userInfo, userInfoTokenVaiTime, TimeUnit.SECONDS);
    }

    @Override
    public void setJwtByUUID(String uuid, String jwt, int userInfoTokenVaiTime) {
        template.opsForValue().set(uuid, jwt, userInfoTokenVaiTime, TimeUnit.SECONDS);
    }

    @Override
    public void expire(String joinedKey, int time) {
        template.expire(joinedKey, time, TimeUnit.SECONDS);
    }

    @Override
    public String createToken(String UUIDString) {
        return VerifyCodeEnum.LOGIN_VERIFY_CODE.join(UUIDString);
    }

    @Override
    public String getUserInfoByToken(String token2) {
        // 传来的就已经是拼接之后的了
        return template.opsForValue().get(token2);
    }

    @Override
    public void set(String joinedKey, String value) {
        template.opsForValue().set(joinedKey, value);
    }

    @Override
    public String get(String key) {
        return template.opsForValue().get(key);
    }

    @Override
    public String getByKey(String key) {
        return template.opsForValue().get(VerifyCodeEnum.LOGIN_VERIFY_CODE.join(key));
    }

    @Override
    public boolean hasKey(String key) {
        return Boolean.TRUE.equals(template.hasKey(key));
    }

    @Override
    public void expireTime(String token) {
        template.expire(VerifyCodeEnum.LOGIN_VERIFY_CODE.join(token), Consts.USER_INFO_TOKEN_VAI_TIME * 60, TimeUnit.SECONDS);
    }

    @Override
    public Long getTime(String token) {
        return template.opsForValue().getOperations().getExpire(token);
    }


    @Override
    public void setValueAndTime(String key, String value, Long minute) {
        template.opsForValue().set(VerifyCodeEnum.REGISTER_VERIFY_CODE.join(key), value, minute, TimeUnit.SECONDS);
    }

    @Override
    public Object getSplicedKey(String key) {
        return template.opsForValue().get(key);
    }

    @Override
    public Boolean deleteKey(String key) {
        return template.delete(key);
    }
}
