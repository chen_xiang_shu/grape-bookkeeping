package com.putao.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseVo {
    public static final String ENV_PHONE = "PHONE";
    public static final String ENV_PC = "PC";

    private int pageSize = 10;      // 每页条数
    private int pageIndex = 1;      // 第几页
    private String search;          // 模糊查询
    private String columnName;      // 下拉框对应表的字段名
    private String select;          // 下拉选择过滤
    private String userId;          // 用户id
    private String env;             // 环境 用于区分是手机端，还是移动端 base64需要解密
    private String parentId;        // 用于接收分页时的条件id，例如账单移动端查询数据时，需要查出某个账本下的，这个id就是账本id
    private String month;           // 月份
    private String year;            // 年份
}
