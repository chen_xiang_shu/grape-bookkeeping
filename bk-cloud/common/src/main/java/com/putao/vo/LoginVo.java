package com.putao.vo;

import com.putao.domain.SysUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LoginVo {
    private String jwt;
    private String uuid;
    private SysUser sysUser;
}
