package com.putao.vo;

import com.putao.domain.SysUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserVO extends SysUser {
    private String repassword;
    private String code;
}
