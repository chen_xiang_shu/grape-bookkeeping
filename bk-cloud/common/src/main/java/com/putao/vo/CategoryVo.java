package com.putao.vo;

import com.putao.domain.Bill;
import com.putao.domain.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryVo extends Category {
    private List<Bill> billList;
}
