package com.putao.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountBillDataVo {
    private List<AccountBillVo> billVoList;
}
