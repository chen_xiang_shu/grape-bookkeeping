package com.putao.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BillRecordVo extends BaseVo{
    // 连续天数
    private Integer continuityDay;
    // 总天数
    private Integer totalDay;
    // 总笔数
    private Integer totalBills;
}
