package com.putao.vo;

import com.putao.domain.AccountBook;
import com.putao.domain.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountBookVo extends AccountBook {

    /**
     * 支出总和
     */
    private BigDecimal payMoneys;
    /**
     * 收入总和
     */
    private BigDecimal incomeMoneys;
    /**
     * 支出集合
     */
    private List<CategoryVo> payCategoryList = new ArrayList<>();

    /**
     * 收入集合
     */
    private List<CategoryVo> incomeCategoryList = new ArrayList<>();
}
