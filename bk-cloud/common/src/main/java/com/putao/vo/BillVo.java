package com.putao.vo;

import com.putao.domain.Bill;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class BillVo extends Bill {
    // 旧的值 (修改时，数据库的值)
    private BigDecimal oldValue;
    // 新的值 (修改时，前端传来的值)
    private BigDecimal newValue;
}
