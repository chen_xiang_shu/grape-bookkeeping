package com.putao.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
public class OperateRecordVO extends BaseVo{
}
