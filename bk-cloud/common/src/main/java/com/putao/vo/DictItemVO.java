package com.putao.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DictItemVO extends BaseVo{
    private String dictId;            // 当在字典类型中点击了某个类型后的请求参数
    private String name;            // 字典值名称
    private Integer totalNum;       // 数据总条数
}
