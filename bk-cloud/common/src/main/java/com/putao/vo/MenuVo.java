package com.putao.vo;

import com.putao.domain.SysMenu;
import lombok.Data;

import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
public class MenuVo extends SysMenu {
    private List<MenuVo> children;
}
