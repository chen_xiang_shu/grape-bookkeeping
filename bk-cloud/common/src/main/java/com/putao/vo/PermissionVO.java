package com.putao.vo;

import com.putao.domain.SysPermission;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
public class PermissionVO extends SysPermission {
}
