package com.putao.vo;

import com.putao.domain.SysRole;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@NoArgsConstructor
@Data
public class RoleVO extends SysRole {
}
