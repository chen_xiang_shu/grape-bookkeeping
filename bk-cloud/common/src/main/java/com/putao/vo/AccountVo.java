package com.putao.vo;

import com.putao.domain.Account;
import com.putao.domain.Bill;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountVo extends Account {
    private List<Bill> billList;
    private String names;
    private BigDecimal moneys;
    private List<AccountBillVo> billVoList;
}
