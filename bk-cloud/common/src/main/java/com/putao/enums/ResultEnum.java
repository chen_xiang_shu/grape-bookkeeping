package com.putao.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public enum ResultEnum {

    /**
     * 查询相关
     */

    SELECT_SUCCESS(true, 10000, "查询成功"),
    SELECT_NOT_FOUND(false, 10009, "找不到数据"),

    /**
     * 新增相关
     */
    INSERT_SUCCESS(true, 20000, "新增成功"),

    /**
     * 新增相关
     */
    UPDATE_SUCCESS(true, 30000, "修改成功"),
    UPDATE_NOT_FOUND(false, 30009, "找不到数据"),

    /**
     * 删除相关
     */
    DELETE_SUCCESS(true, 40000, "删除成功"),
    DELETE_NOT_FOUND(false, 40009, "找不到数据"),
    DELETE_STATUS_ERROR(false, 40008, "状态不满足删除条件"),

    /**
     * 认证相关
     */
    AUTHENTICATION_ERROR(false, 50009, "未登录，请先登录"),
    AUTHENTICATION_NO_PERMISSION(false, 50001, "没有权限"),
    INVALID_TOKEN(false, 50002, "token已过期，请刷新或者重新登录"),


    /**
     * 操作成功
     */
    OPERATION_SUCCESS(true, 60000, "操作成功"),
    OPERATION_ERROR(false, 60009, "操作失败"),

    /**
     * token相关
     */
    REFRESH_SUCCESS(true, 70000, "刷新成功"),
    REFRESH_ERROR(false, 70009, "刷新失败"),

    /**
     * 校验数据
     */
    VERIFY_PARAMETER_ERROR(false, 80000, "缺少参数，数据校验失败"),


    /**
     * 系统异常
     */
    CALL_BACK_DATA(false, 90000, "兜底数据"),
    SYSTEM_ERROR(false, 99999, "系统内部错误"),
    LOGOUT_SUCCESS(true, 70001, "退出成功");


    @Setter @Getter
    private Boolean success;
    @Setter @Getter
    private int code;
    @Setter @Getter
    private String message;

    ResultEnum(Boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }
}
