package com.putao.enums;



public enum SystemCodeEnum {

    PARAMETER_ERROR(false, 50000,"参数不合法"),
    TOKEN_ERROR(false, 50001,"用户未认证"),
    JWT_TIME_OUT_ERROR(false, 40003,"jwt过期,token没过期了，需要刷新"),
    TOKEN_TIME_OUT_ERROR(false, 40004,"jwt过期,token也过期了，需要重新登录"),
    TOKEN_FORMAT_ERROR(false, 40009,"token格式错误"),
    NO_THIS_USER(false, 40005,"用户不存在"),
    SYSTEM_INNER_ERROR(false, 50000, "系统内部错误"),
    USER_EXISTS(true, 50001, "用户存在"),                                                   // 扫码登录的返回
    USER_NOT_EXISTS(false, 50002, "用户不存在"),                                             // 用户不存在，则弹框绑定手机号码
    WEBSOCKET_CONNECT_SUCCESS(true, 50003, "连接成功"),                              // 连接成功
    REFRESH_JWT_NOT_EQUALS_ERROR(false, 50004, "刷新时jwt不相同错误"),                // 刷新时jwt不相同错误
    REFRESH_JWT_NOT_EXPIRE(false, 50004, "刷新时jwt未过期错误"),                     // 刷新时jwt未过期错误
    REFRESH_USERNAME_NOT_EQUALS_ERROR(false, 50004, "刷新时用户名不相同错误"),        // 刷新时用户名不相同错误
    INSERT_USER_USERNAME_REPEAT(false, 50005, "该用户名已被使用"),                   // 该用户名已被使用
    INSERT_USER_TWICE_PASSWORD_ERROR(false, 50005, "两次密码不一致"),                // 两次密码不一致
    INSERT_PHONE_EXISTS(false, 50005, "手机号码已存在"),                             // 手机号码已存在
    INSERT_PHONE_FORMAT_ERROR(false, 50005, "手机格式错误"),                         // 手机格式错误
INSERT_CODE_ERROR(false, 50005, "验证码错误"),                                           // 验证码错误

    VERIFY_PARAMS_EMPTY_ERROR(false, 55555, "参数为空错误"),                              // 参数为空

    INSERT_VALUE_REPEAT(false, 50006, "新增时数据重复"),
    REQUEST_PARSE_JSON_OBJECT_ERROR(false, 50007, "请求时返回token异常"),                  // 请求时返回token异常
    REQUEST_DATA_ERROR(false, 50008, "post请求参数异常"),                                 // 请求时参数异常
    ACCOUNT_BOOK_NUMBER_OVER(false, 50009, "账本最多只能五个"),                             // 请求时参数异常
    ACCOUNT_BOOK_NAME_FORMAT_ERROR(false, 50010, "账本名字异常，最多五个字符"),                             // 账本名字长度过长，只能五个字符
    ACCOUNT_NAME_FORMAT_ERROR(false, 50010, "账户名字异常，最多七个字符");                             // 账本名字长度过长，只能五个字符

    /** 是否成功 */
    private boolean success;

    /** 错误码 */
    private int errorCode;

    /** 错误描述 */
    private String errorMsg;

    SystemCodeEnum(boolean success, int errorCode, String errorMsg) {
        this.success = success;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    SystemCodeEnum() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }


    public Boolean getSuccess() {
        return this.success;
    }
}
