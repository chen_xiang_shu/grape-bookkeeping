package com.putao.enums;

import com.putao.constants.Consts;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;


public enum VerifyCodeEnum {
    REGISTER_VERIFY_CODE("register_verify_code", Consts.VERIFY_CODE_VAI_TIME * 60L),
    VERIFY_CODE_KEY("code", 180L),
    QR_CODE_TIME_OUT("qr_code", 120L),
    OPERATE_LOG_IGNORE_URL("operate_log_ignore_url", 120000L),
    LOGIN_VERIFY_CODE("login_verify_code", Consts.VERIFY_CODE_VAI_TIME * 180L);

    // 两个属性
    @Setter@Getter
    private String prefix;      // key的前缀
    @Setter@Getter
    private Long time;        // key的有效时间

    VerifyCodeEnum(String prefix, Long time) {
        this.prefix = prefix;
        this.time = time;
    }

    /**
     * 拼接 验证码存入redis后的key,例如    register_verify_code:14902393274
     * @param values
     * @return
     */
    public String join(String... values) {
        StringBuilder stringBuilder = new StringBuilder(80);
        stringBuilder.append(this.prefix);
        Arrays.stream(values).forEach(value -> stringBuilder.append(":" + value));
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        System.out.println(VerifyCodeEnum.REGISTER_VERIFY_CODE.join("j"));
    }
}
