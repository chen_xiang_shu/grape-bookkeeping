package com.putao.base;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.putao.utils.AssertUtil;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public class BaseWrapper<T> extends QueryWrapper<T> {
    public BaseWrapper() {
        this(null);
    }

    public BaseWrapper(T entity) {
        super.setEntity(entity);
        super.initNeed();
    }

    public BaseWrapper(T entity, String... columns) {
        super.setEntity(entity);
        super.initNeed();
        this.select(columns);
    }

    public QueryWrapper<T> eq(String columnName, String value) {
        AssertUtil.hasText(value, "where条件时值不能为空");
        return super.eq(columnName, value);
    }

    public QueryWrapper<T> init() {
        BaseWrapper<T> wrapper = new BaseWrapper<>();
        return wrapper.eq("del_flag", BaseDomain.DEL_FLAG_NORMAL);
    }

    public QueryWrapper<T> init(String userId) {
        QueryWrapper<T> wrapper = new BaseWrapper<>();
        return wrapper.eq("del_flag", BaseDomain.DEL_FLAG_NORMAL).eq("create_id", userId);
    }
}
