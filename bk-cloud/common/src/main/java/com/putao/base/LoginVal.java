package com.putao.base;

import lombok.Data;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
public class LoginVal {

    private String userId;

    private String username;

    private String[] authorities;

    private String nickname;

    private Integer gender;

    private String avatar;

    private String mobile;

    private String email;

    /**
     * JWT令牌唯一ID
     */
    private String jti;

    /**
     * 过期时间，单位秒，距离过期时间还有多少秒
     */
    private Long expireIn;
}
