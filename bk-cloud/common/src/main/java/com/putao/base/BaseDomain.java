package com.putao.base;

import cn.hutool.core.util.IdUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.putao.utils.ThreadLocalUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseDomain {

    private String id;


    /**
     * 删除标记 0：正常，1：删除
     */
    private String delFlag = BaseDomain.DEL_FLAG_NORMAL;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /**
     * 新增人id
     */
    private String createId;

    /**
     * 修改人id
     */
    private String modifyId;

    /**
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date modifyTime;

    public static String DEL_FLAG_DELETE = "1";
    public static String DEL_FLAG_NORMAL = "0";

    /** 自定义方法区 */
    public void insertInit(){
        this.id = IdUtil.getSnowflake(31, 31).nextIdStr();
        this.delFlag =  STATUS_NORMAL;
        this.createTime = new Date();
        this.createId = ThreadLocalUtil.getUserId();
        this.modifyId = ThreadLocalUtil.getUserId();
        this.modifyTime = new Date();
    }


    public void insertInit(String id){
        this.id = id;
        this.delFlag =  STATUS_NORMAL;
        this.createTime = new Date();
        this.createId = ThreadLocalUtil.getUserId();
        this.modifyId = ThreadLocalUtil.getUserId();
        this.modifyTime = new Date();
    }

    /** 自定义方法区 */
    public void updateInit(){
        this.modifyTime = new Date();
        this.modifyId = ThreadLocalUtil.getUserId();
    }

    /** 状态 */
    public static final String STATUS_NORMAL = "0";
    public static final String STATUS_ERROR = "1";
 }
