package com.putao.base;

import lombok.Data;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
public abstract class BaseColor extends BaseDomain{

    /**
     * 卡片背景色
     */
    private String lightColor;

    /**
     * 回显颜色，深色
     */
    private String bgcColor;

    /**
     * 图标颜色
     */
    private String iconColor;

    /**
     * 图标背景圆颜色
     */
    private String circularColor;

}
