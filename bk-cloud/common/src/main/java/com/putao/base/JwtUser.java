package com.putao.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JwtUser {
    private String id;
    private String username;
    private String avatar;
    private String jti;
    private String phone;
    private String email;
}
