package com.putao.error;

import lombok.Data;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
public class BusinessException  extends RuntimeException{

    //直接构造错误消息的构造异常
    public BusinessException(String message){
        super(message);
    }
}
