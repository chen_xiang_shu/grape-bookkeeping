package com.putao.other;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Configuration
public class AccountTypeConfiguration {

    @Bean
    public List<AccountType> accountTypeList() {
        List<AccountType> accountTypeList = new ArrayList<>();
        List<AccountInner> accountInnerList = new ArrayList<>();
        accountInnerList.add(new AccountInner("1", "银行卡", "'icon-yinhangqia", "#c5616a"));
        accountInnerList.add(new AccountInner("2", "现金i", "icon-zhanghuyue", "#f8d971"));
        accountInnerList.add(new AccountInner("3", "支付宝", "icon-zhifubaozhifu", "#4e80ea"));
        accountInnerList.add(new AccountInner("4", "微信钱包", "icon-weixinqianbaocopy", "#4aaf36"));
        accountInnerList.add(new AccountInner("5", "公积金", "icon-gongjijinchaxun", "#3e9877"));
        accountInnerList.add(new AccountInner("6", "社保", "icon-shebaochaxun", "#4f70ee"));
        accountInnerList.add(new AccountInner("7", "其他现金账户", "icon-xianxiazijin'", "#e1ad51"));
        accountTypeList.add(new AccountType("1", "资金账户", accountInnerList));

        List<AccountInner> accountInnerList2 = new ArrayList<>();
        accountInnerList2.add(new AccountInner("1", "股票", "icon-gupiao-copy", "#f0b744"));
        accountInnerList2.add(new AccountInner("2", "基金", "icon-jijin", "#5e57e2"));
        accountInnerList2.add(new AccountInner("3", "虚拟货币", "icon-tubiaozhizuo-02-01", "#4e569b"));
        accountInnerList2.add(new AccountInner("4", "固定资产", "icon-gudingzichanzhuangushenqingliucheng-03", "#419877"));
        accountTypeList.add(new AccountType("2", "信用账户", accountInnerList2));

        List<AccountInner> accountInnerList3 = new ArrayList<>();
        accountInnerList3.add(new AccountInner("1", "股票", "icon-gupiao-copy", "#f0b744"));
        accountInnerList3.add(new AccountInner("2", "基金", "icon-jijin", "#5e57e2"));
        accountInnerList3.add(new AccountInner("3", "虚拟货币", "icon-tubiaozhizuo-02-01", "#4e569b"));
        accountInnerList3.add(new AccountInner("4", "固定资产", "icon-gudingzichanzhuangushenqingliucheng-03", "#419877"));
        accountTypeList.add(new AccountType("3", "投资", accountInnerList3));


        List<AccountInner> accountInnerList4 = new ArrayList<>();
        accountInnerList4.add(new AccountInner("1", "会员卡", "icon-huiyuankax", "#ddb151"));
        accountInnerList4.add(new AccountInner("2", "公交卡", "icon-gongjiaoqiachongzhi", "#e16f3f"));
        accountInnerList4.add(new AccountInner("3", "校园卡", "icon-iconcopy", "#5e59e2"));
        accountInnerList4.add(new AccountInner("4", "其他充值账户", "icon-chongzhi", "#d15c4a"));
        accountTypeList.add(new AccountType("4", "'充值账户'", accountInnerList4));

        return accountTypeList;
    }

}
