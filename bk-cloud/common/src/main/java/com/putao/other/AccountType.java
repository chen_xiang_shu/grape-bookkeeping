package com.putao.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountType {
    private String id;
    private String name;
    private List<AccountInner> accountInnerList;
}
