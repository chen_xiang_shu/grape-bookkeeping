package com.putao.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountInner {
    private String id;
    private String name;
    private String icon;
    private String color;
}
