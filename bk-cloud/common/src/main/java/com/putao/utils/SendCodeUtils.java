package com.putao.utils;

import com.putao.redis.service.IRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.putao.enums.VerifyCodeEnum.VERIFY_CODE_KEY;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Component
public class SendCodeUtils {

    @Autowired
    private IRedisService redisService;

    @Async
    public Boolean sendCode(String phone, String code, Date date) {
        // 使用腾讯云发送短信
        //SendPhoneCodeUtils.sendCode(code, phone);
        redisService.setAndTime(VERIFY_CODE_KEY.join(phone), code, VERIFY_CODE_KEY.getTime());
        Date endTime = new Date();
        long second = (endTime.getTime() - date.getTime()) / 1000;
        System.out.println("second = " + second);
        System.out.println("sysUser = " + ThreadLocalUtil.get());
        return true;
    }

    public String getCode(String phone) {
        return redisService.get(VERIFY_CODE_KEY.join(phone));
    }
}
