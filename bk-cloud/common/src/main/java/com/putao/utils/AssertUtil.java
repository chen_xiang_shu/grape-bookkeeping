package com.putao.utils;

import cn.hutool.core.util.ObjectUtil;
import com.putao.error.BusinessException;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.regex.Pattern;


/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public abstract class AssertUtil {

    /**
     * 判断参数是否为null, 或者是否为 ''，如果是则抛出异常，提示信息是传来的message
     * @param text
     * @param message
     */
    public static void hasText(Object text, String message) {
        if (ObjectUtil.isEmpty(text) || !StringUtils.hasText(text.toString())) {
            throw new BusinessException(message);
        }
    }


    /**
     * 正则验证手机号码
     * @param phone
     * @param message
     */
    public static void checkPhoneFormat(String phone, String message) {
        // 首先获取使用pattern对象的compile获取pattern对象，注意该构造器是有参构造，并且是正则表达式
        // 然后调用matcher方法，传入需要匹配的字符串，调用matches方法执行匹配
        String regExp = "^((13[0-9])|(15[^4])|(18[0,2,3,5-9])|(17[0-8])|(147))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        if (!p.matcher(phone).matches()) {
            throw new BusinessException(message);
        }
    }

    /**
     * 判断新增通知中传进来的各个时间段是否正确
     * @param morningTime 6:00 --8:00
     * @param afternoonTime 13:00 --15:00
     * @param nightTime 20:00 -- 22:00
     */
    public static void timeIsInvalid(Date morningTime, Date afternoonTime, Date nightTime) throws ParseException {
        final LocalTime morningStart = LocalTime.of(6, 0, 0, 0);
        final LocalTime morningEnd = LocalTime.of(8, 0, 0, 0);
        final LocalTime afternoonStart = LocalTime.of(13, 0, 0, 0);
        final LocalTime afternoonEnd = LocalTime.of(15, 0, 0, 0);
        final LocalTime nightStart = LocalTime.of(20, 0, 0, 0);
        final LocalTime nightEnd = LocalTime.of(22, 0, 0, 0);
//        if (!isEffectiveTime(morningTime,morningStart,morningEnd) ||
//                !isEffectiveTime(afternoonTime,afternoonStart,afternoonEnd) ||
//                !isEffectiveTime(nightTime,nightStart,nightEnd)) {
//            throw new NoticeTimeException("设置时间错误");
//        }

    }
    public static boolean isEffectiveTime(Date targetTime,LocalTime startTime,LocalTime endTime) throws ParseException {
        String newSTR = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String format = new SimpleDateFormat("HH:mm:ss").format(targetTime);
        String SAT = newSTR +' '+ format;
        Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(SAT);
        Instant instant = parse.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalTime time = LocalDateTime.ofInstant(instant, zone).toLocalTime();
        return time.isAfter(startTime) && time.isBefore(endTime);

    }

    public static void main(String[] args) throws ParseException {

        String newSTR = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String format = new SimpleDateFormat("HH:mm:ss").format(new Date());
        String SAT = newSTR +' '+ format;
        System.err.println(SAT);
        Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(SAT);
        System.out.println(parse);
        LocalTime time1 = LocalTime.of(0, 20, 0);
        LocalTime time2 = LocalTime.of(0, 30, 0);
        System.out.println(isEffectiveTime(parse, time1, time2));

    }
}
