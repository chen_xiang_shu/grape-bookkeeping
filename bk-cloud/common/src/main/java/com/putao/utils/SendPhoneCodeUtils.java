package com.putao.utils;

import com.alibaba.fastjson.JSONException;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import lombok.extern.slf4j.Slf4j;

import javax.xml.ws.http.HTTPException;
import java.lang.reflect.Field;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Slf4j
public class SendPhoneCodeUtils {

    // 签名
    String smsSign = "程序员葡萄"; // NOTE: 这里的签名"腾讯云"只是一个示例，真实的签名需要在短信控制台中申请，另外签名参数使用的是`签名内容`，而不是`签名ID`


    /**
     * 自定义短信内容发送
     * @param code 短信内容
     * @param phone 用户手机号
     * @return OK 成功  null 失败
     */
    public static boolean sendCode(String code, String phone) {
        // TODO 更改为自己的AppId, AppKey, templateId
        // 短信应用SDK AppID
        int appid = 0; // 1400开头

        // 短信应用SDK AppKey
        String appkey = "appkey";

        // 短信模板ID，需要在短信应用中申请 templateId
        int templateId = 0; // NOTE: 这里的模板ID`7839`只是一个示例，真实的模板ID需要在短信控制台中申请

        // 需要发送短信的手机号码
        String[] phoneNumbers = {phone};
        //templateId7839对应的内容是"您的验证码是: {1}"
        String smsSign = "程序员葡萄"; // NOTE: 这里的签名"腾讯云"只是一个示例，真实的签名需要在短信控制台中申请，另外签名参数使用的是`签名内容`，而不是`签名ID`

        try {
            String[] params = {code};//数组具体的元素个数和模板中变量个数必须一致，例如事例中templateId:5678对应一个变量，参数数组中元素个数也必须是一个
            SmsSingleSender ssender = new SmsSingleSender(appid, appkey);
            SmsSingleSenderResult result = ssender.sendWithParam("86", phoneNumbers[0],
                    templateId, params, smsSign, "", "");  // 签名参数未提供或者为空时，会使用默认签名发送短信
            System.out.println(result);
            Class<? extends SmsSingleSenderResult> aClass = result.getClass();
            // 获取类中声明的字段
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                // 避免 can not access a member of class com.java.test.Person with modifiers "private"
                field.setAccessible(true);
                try {
                    if ("result".equals(field.getName())) {
                        if (Integer.parseInt(field.get(result) + "") == 0) {
                            log.error("短信发送成功,验证码是 " + code);
                            return true;
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        } catch (HTTPException e) {
            // HTTP响应码错误
            e.printStackTrace();
        } catch (JSONException e) {
            // json解析错误
            e.printStackTrace();
        } catch (Exception e) {
            // 网络IO错误
            e.printStackTrace();
        }
        return false;
    }

}
