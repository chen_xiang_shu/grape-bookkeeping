package com.putao.utils;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;
import com.putao.domain.SysMenu;
import com.putao.vo.MenuVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 类描述： Controller类
 *
 * @author fuhehuang
 * @version 1.0
 * @address
 * @email fhehuang@163.com
 * @date 2022/1/18 13:57
 */
public abstract class MenuUtils {

//    public static MenuVo buildTree(List<SysDepartment> departmentList){
//        if (departmentList.isEmpty() || departmentList.size() == 0){
//            return new MenuVo();
//        }
//        Map<Long, MenuVo> departmentMap = departmentList.stream().collect(Collectors.toMap(SysDepartment::getId, department -> new MenuVo(department.getId(), department.getName(), department.getParentId(), new ArrayList<>())));
//        List<MenuVo> menuVoList = departmentList.stream().map(department -> new MenuVo(department.getId(), department.getName(), department.getParentId(), new ArrayList<>())).collect(Collectors.toList());
//        for (MenuVo menuVo : menuVoList) {
//            menuVo.setChildren(getList(menuVo, departmentMap));
//        }
//        return menuVoList.get(0);
//    }
//
//    /**
//     *  @param menuVo
//     */
//    private static List<MenuVo> getList(MenuVo menuVo, Map<Long, MenuVo> sysDepartmentMap) {
//        List<MenuVo> children = menuVo.getChildren();
//        sysDepartmentMap.forEach(new BiConsumer<Long, MenuVo>() {
//            @Override
//            public void accept(Long aLong, MenuVo menuVo1) {
//                if (menuVo.getId().equals(menuVo1.getParentId())){
//                    getList(menuVo, sysDepartmentMap);
//                }
//            }
//        });
//        return children;
//    }

    public static List<MenuVo> getTree(List<MenuVo> nodes) {
        Map<String, MenuVo> nodeMap = Maps.newHashMap();
        List<MenuVo> rootList = Lists.newArrayList();
        for (MenuVo node : nodes) {
            nodeMap.put(node.getId(), node);
            String parentId = node.getParentId();
            if (StringUtils.isNotBlank(parentId)) {
                rootList.add(node);
            }
        }
        for (MenuVo node : nodes) {
            // 遍历的时候根据遍历的元素，从map中获取出对应的对象（元素的id = map元素的parentId）
            String parentId = node.getParentId();
            if (StringUtils.isNotBlank(parentId)) {
                continue;
            }
            MenuVo pnode = nodeMap.get(parentId);
            if (pnode == null) {
                continue;
            }
            List<MenuVo> children = pnode.getChildren();
            if (children == null) {
                children = Lists.newArrayList();
                pnode.setChildren(children);
            }
            children.add(node);
        }
        return rootList;
    }

    /**
     * 内部方法-递归获取获取菜单树(核心是设置子菜单集)
     * @param parentId 父栏菜单id
     * @param menus
     * @return
     */
    public static List<MenuVo> recursionSysMenu(String parentId, List<SysMenu> menus) {
        List<MenuVo> children = new ArrayList<>();
        for (SysMenu menu : menus) {
            // 递归出口-如果当前是子菜单
            if (parentId.equals(menu.getParentId())){
                MenuVo child = new MenuVo();
                BeanUtil.copyProperties(menu, child);
                child.setChildren(recursionSysMenu(menu.getId(), menus));
                children.add(child);
            }
        }
        return children;
    }

    public static void main(String[] args) {
        List<MenuVo> recursionMenus = recursionSysMenu("0", generateTestData());
        System.out.println("递归获取菜单树:" + JSONObject.toJSONString(recursionMenus));
    }

    /**
     * 内部方法-生成测试数据
     */
    private static List<SysMenu> generateTestData() {
        List<SysMenu> sysMenus = new ArrayList<>();
        SysMenu sysMenu = new SysMenu();
        sysMenu.setId("1");
        sysMenu.setParentId("0");
        sysMenus.add(sysMenu);

        SysMenu sysMenu2 = new SysMenu();
        sysMenu2.setId("2");
        sysMenu2.setParentId("0");
        sysMenus.add(sysMenu2);

        SysMenu sysMenu3 = new SysMenu();
        sysMenu3.setId("3");
        sysMenu3.setParentId("1");
        sysMenus.add(sysMenu3);

        SysMenu sysMenu4 = new SysMenu();
        sysMenu4.setId("4");
        sysMenu4.setParentId("2");
        sysMenus.add(sysMenu4);

        SysMenu sysMenu5 = new SysMenu();
        sysMenu5.setId("5");
        sysMenu5.setParentId("1");
        sysMenus.add(sysMenu5);

        SysMenu sysMenu6 = new SysMenu();
        sysMenu6.setId("6");
        sysMenu6.setParentId("1");
        sysMenus.add(sysMenu6);

        SysMenu sysMenu7 = new SysMenu();
        sysMenu7.setId("7");
        sysMenu7.setParentId("1");
        sysMenus.add(sysMenu7);

        SysMenu sysMenu8 = new SysMenu();
        sysMenu8.setId("8");
        sysMenu8.setParentId("4");
        sysMenus.add(sysMenu8);

        SysMenu sysMenu9 = new SysMenu();
        sysMenu9.setId("9");
        sysMenu9.setParentId("3");
        sysMenus.add(sysMenu9);
        return sysMenus;
    }

}
