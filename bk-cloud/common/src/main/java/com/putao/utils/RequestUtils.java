package com.putao.utils;

import com.putao.constants.Constants;
import com.putao.domain.SysOperateLog;
import com.putao.domain.SysUser;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Slf4j
public class RequestUtils {
    public static List<String> getSystemInfo(HttpServletRequest request){
        String ipAddress = SystemUtils.getIpAdrress(request);
        String browserName = SystemUtils.getBrowserName(request).replace(" ", "");
        String osName = SystemUtils.getOsName(request).replace(" ", "");
        List<String> list = new ArrayList<>();
        list.add(ipAddress);
        list.add(browserName);
        list.add(osName);
        log.error("拦截器中输出 真实ip：" + ipAddress);
        log.error("拦截器中输出 浏览器：" + browserName);
        log.error("拦截器中输出 操作系统：" + osName);
        return list;
    }

    public static void getSystemInfo(HttpServletRequest request, SysOperateLog operateLog){
        String ipAddress = SystemUtils.getIpAdrress(request);
        String browserName = SystemUtils.getBrowserName(request).replace(" ", "");
        String osName = SystemUtils.getOsName(request).replace(" ", "");
        operateLog.setCreateTime(new Date());
        operateLog.setIp(ipAddress);
    }

    /**
     * 根据请求对象判断类型
     * @param request
     * @return
     */
    public static boolean checkAgentIsMobile(HttpServletRequest request){
        //定义移动端请求可能的设备类型
        String[] agent = { "Android", "iPhone", "iPod","iPad", "Windows Phone", "MQQBrowser" };
        String agentStr = request.getHeader("User-Agent");
        boolean flag = false;
        if (!agentStr.contains("Windows NT") || (agentStr.contains("Windows NT") && agentStr.contains("compatible; MSIE 9.0;"))) {
            // 排除 苹果桌面系统
            if (!agentStr.contains("Windows NT") && !agentStr.contains("Macintosh")) {
                for (String item : agent) {
                    if (agentStr.contains(item)) {
                        flag = true;
                        break;
                    }
                }
            }
        }
        return flag;
    }

    /**
     * 根据请求对象获取token
     * @return
     */
    public static String getToken(HttpServletRequest request) {
        return request.getHeader("Authorization");
    }

    /**
     * 根据请求对象获取token
     * @return
     */
    public static String getEnv(HttpServletRequest request) {
        return request.getHeader("env");
    }

    /**
     * 获取头部token字段值
     * @return
     */
    public static String getTokenHeader() {
        return Constants.TOKEN_HEADER;
    }

    public static String getUUID(HttpServletRequest request) {
        return request.getHeader("uuid");
    }

    /**
     * 根据请求对象从请求头中取出用户id，并创建用户对象，设置userId
     * @param request
     * @return
     */
    public static SysUser getUser(HttpServletRequest request) {
        String userId = request.getHeader("userId");
        SysUser sysUser = new SysUser();
        sysUser.setId(userId);
        return sysUser;
    }
}
