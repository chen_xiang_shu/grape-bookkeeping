package com.putao.utils;

import cn.hutool.core.util.ObjectUtil;
import com.putao.base.JwtUser;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public class ThreadLocalUtil {
    private ThreadLocalUtil() {
    }

    private static ThreadLocal<JwtUser> THREAD_LOCAL_FRONT_USER = new ThreadLocal<>();

    public static void set(JwtUser user) {
        THREAD_LOCAL_FRONT_USER.set(user);
    }

    public static JwtUser get() {
        return THREAD_LOCAL_FRONT_USER.get();
    }

    /**
     * 获取用户id
     * @return
     */
    public static String getUserId() {
        if (ObjectUtil.isNotNull(get()))  {
            return get().getId();
        } else {
            return null;
        }
    }

    /**
     * 获取名字
     * @return 用户名 username
     */
    public static String getUserName() {
        if (ObjectUtil.isNotNull(get()))  {
            return get().getUsername();
        } else {
            return null;
        }
    }

    public static void remove() {
        THREAD_LOCAL_FRONT_USER.remove();
    }
}
