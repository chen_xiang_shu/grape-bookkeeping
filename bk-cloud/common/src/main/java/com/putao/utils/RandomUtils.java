package com.putao.utils;

import java.util.Random;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public class RandomUtils {
    public static String generateCode(){
        Random random = new Random();
        String fourRandom = random.nextInt(10000) + "";
        int randLength = fourRandom.length();
        if(randLength < 4){
            for(int i = 1; i <= 4 - randLength; i++)
                fourRandom = "0" + fourRandom ;
        }
        return fourRandom;
    }
}
