package com.putao.domain;

import com.putao.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysDict extends BaseDomain implements Serializable {


    private static final long serialVersionUID=1L;


    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型（例如：sys_dict_sex）
     */
    private String type;

    /**
     * 字典编码
     */
    private Integer dictCode;

    /**
     * 描述
     */
    private String description;

    /**
     * 备注
     */
    private String remarks;


}
