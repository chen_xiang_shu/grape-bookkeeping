package com.putao.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.putao.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysRole extends BaseDomain implements Serializable {

    private static final long serialVersionUID=1L;

    private String name;

    private String code;

    private String permissionIds;

    @TableField(exist=false)
    private List<SysPermission> list = new ArrayList<>();
}
