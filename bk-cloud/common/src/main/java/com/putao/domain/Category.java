package com.putao.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.putao.base.BaseColor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_category")
public class Category extends BaseColor implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 分类名字
     */
    private String name;

    /**
     * 分类图标
     */
    private String icon;

    /**
     * 预算
     */
    private BigDecimal budget;


    /**
     * 颜色索引-用于新增，回显
     */
    private int colorIndex = -1;

    /**
     * 图标所在分类索引
     */
    private int categoryIndex = -1;

    /**
     * 所在分类下的图标集合索引
     */
    private int iconIndex = -1;

    /**
     * 余额
     */
    private BigDecimal balance;

    /**
     * 类型（pay: 支出, income: 收入）
     */
    private String type;

    /**
     * 账本id
     */
    private String accountBookId;

    /**
     * 所属账本名字
     */
    private String accountBookName;

    /** 比例 */
    @TableField(exist = false)
    private String ratio = "0%";
}
