package com.putao.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.putao.base.BaseDomain;
import com.putao.error.BusinessException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.logging.log4j.util.Strings;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_bill")
public class Bill extends BaseDomain implements Serializable {

    public static final Map<Integer, String> weekMap = new HashMap<>();
    static {
        weekMap.put(1, "周日");
        weekMap.put(2, "周一");
        weekMap.put(3, "周二");
        weekMap.put(4, "周三");
        weekMap.put(5, "周四");
        weekMap.put(6, "周五");
        weekMap.put(7, "周六");
    }

    private static final long serialVersionUID=1L;

    /**
     * 账单类型 PAY：支出 INCOME：收入
     */
    private String type;

    /**
     * 金额

     */
    private BigDecimal money;

    /**
     * 所属分类id
     */
    private String categoryId;

    /**
     * 所属分类名字
     */
    private String categoryName;

    /**
     * 分类类型（pay: 支出, income: 收入）统计时使用
     */
    private String categoryType;

    /**
     * 分类图标
     */
    private String categoryIcon;

    /**
     * 分类图标颜色
     */
    private String categoryIconColor;

    /**
     * 所属账户id
     */
    private String accountId;

    /**
     * 所属账户名字
     */
    private String accountName;

    /**
     * 所属账户图标
     */
    private String accountIcon;

    /**
     * 所属账户图标颜色
     */
    private String accountIconColor;

    /**
     * 所属账本id
     */
    private String accountBookId;

    /**
     * 所属账本名字
     */
    private String accountBookName;

    /**
     * 备注
     */
    private String remarks;
    /**
     * 月份
     */
    private String monthStr;
   /**
     * 年份
     */
    private String yearStr;
   /**
     * 分类索引
     */
    private Integer categoryIndex;
   /**
     * 账户索引
     */
    private Integer accountIndex;
   /**
     * 账本索引
     */
    private Integer accountBookIndex;
   /**
     * 几号
     */
    private String dayStr;
   /**
     * 周几
     */
    private String weekStr;
   /**
     * 几月几号
     */
    private String dayTime;
   /**
     * 分类圆的背景色
     */
    private String categoryCircularColor;
   /**
     * 分秒字符串（mm:dd）格式
     */
    private String minuteSeconds;
    /**
     * 账单时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date time;

    /**
     * 检查月份与日期中的月份是否一致，不一致则抛出异常，
     * 检查年份，待移动端的年份组件完成
     */
    public void checkYearAndMonth() {
        Calendar instance = Calendar.getInstance();
        instance.setTime(this.time);
        int month = instance.get(Calendar.MONTH) + 1;
        int year = instance.get(Calendar.YEAR);
        if (!(year + "").equals(this.yearStr)) {
            throw new BusinessException("年份与时间的年份不对，请修改");
        }
        if (!(month + "").equals(this.monthStr)) {
            throw new BusinessException("月份与时间的月份不对，请修改");
        }
    }

    /**
     * 新增前初始化
     */
    public void initProperties() {
        Calendar instance = Calendar.getInstance();
        instance.setTime(this.time);
        this.dayStr = instance.get(Calendar.DATE) + Strings.EMPTY;
        this.weekStr = weekMap.get(instance.get(Calendar.DAY_OF_WEEK));
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.time);
        String dayTimeStr = format.substring(5, 10);
        String minuteSecondStr = format.substring(11);
        String[] split = dayTimeStr.split("-");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < split.length; i++) {
            if (i == 0) {
                stringBuilder.append(split[i]).append("月");
            } else {
                stringBuilder.append(split[i]).append("日");
            }
        }
        this.dayTime = stringBuilder.toString();
        this.minuteSeconds = minuteSecondStr;
    }
}
