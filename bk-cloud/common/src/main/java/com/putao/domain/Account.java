package com.putao.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.putao.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_account")
public class Account extends BaseDomain implements Serializable {

    private static final long serialVersionUID=1L;

    public static final String ACCOUNT_TYPE_CAPITAL_ACCOUNT = "capitalAccount";                     // 资金账户
    public static final String ACCOUNT_TYPE_CREDIT_ACCOUNT = "creditAccount";                       // 信用账户
    public static final String ACCOUNT_TYPE_INVESTMENT = "investment";                              // 投资
    public static final String ACCOUNT_TYPE_RECHARGE_ACCOUNT = "rechargeAccount";                   // 充值账户


    public static final Map<String, String> typeMap = new HashMap<>();

    static {
        typeMap.put(ACCOUNT_TYPE_CAPITAL_ACCOUNT, "资金账户");
        typeMap.put(ACCOUNT_TYPE_CREDIT_ACCOUNT, "信用账户");
        typeMap.put(ACCOUNT_TYPE_INVESTMENT, "投资");
        typeMap.put(ACCOUNT_TYPE_RECHARGE_ACCOUNT, "充值账户");
    }

    public static String getTypeName(String type) {
        return typeMap.get(type);
    }


    /**
     * 账本名字
     */
    private String name;

    /**
     * 账本图标
     */
    private String icon;

    /**
     * 账户id
     */
    private String accountBookId;

    /**
     * 类型（capitalAccount：资金账户、creditAccount：信用账户、investment：投资，rechargeAccount：充值账户））
     */
    private String type;

    /**
     * 金额：一开始设置的金额
     */
    private BigDecimal money;

    /**
     * 该账户卡号后四位
     */
    private String lastFourDigitsOfCard;

    /**
     * 图标颜色
     */
    private String iconColor;

    /**
     * 类型索引，共有四个类型
     */
    private String typeIndex;

    /**
     * 图标索引（对于所在账户类型中）
     */
    private String iconIndex;
    /**
     * 余额,金额减去对应的账单（如果是支出，减去；收入，加上） 余额 = 金额 - 总数
     */
    private BigDecimal balance;
    /**
     * 总数：总支出，或者总收入
     */
    private BigDecimal totalNum;

    /**
     * 新增或者修改时设置其他属性，
     */
    public void initBalance() {
        this.setBalance(this.money.subtract(this.totalNum));
    }
}
