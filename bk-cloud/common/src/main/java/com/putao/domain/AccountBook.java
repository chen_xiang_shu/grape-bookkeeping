package com.putao.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.putao.base.BaseColor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_account_book")
public class AccountBook extends BaseColor implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 账本名字
     */
    private String name;

    /**
     * 账本图标
     */
    private String icon;

    /**
     * 用户名字
     */
    private String username;

    /**
     * 预算
     */
    private BigDecimal budget;

    /**
     * 颜色索引-用于新增，回显
     */
    private int colorIndex = -1;

    /**
     * 图标所在分类索引
     */
    private int categoryIndex = -1;

    /**
     * 所在分类下的图标集合索引
     */
    private int iconIndex = -1;

    /**
     * 所在分类下的图标集合索引
     */
    private int width = -1;

    /**
     * 是否选中
     */
    private boolean selected;


    /**
     * 账本支出总数
     */
    @TableField(exist = false)
    private BigDecimal payMoney = BigDecimal.ZERO;

    /**
     * 账本收入总数
     */
    @TableField(exist = false)
    private BigDecimal incomeMoney = BigDecimal.ZERO;

    /**
     * 是否显示编辑标记
     */
    @TableField(exist = false)
    private boolean showEditFlag;


}
