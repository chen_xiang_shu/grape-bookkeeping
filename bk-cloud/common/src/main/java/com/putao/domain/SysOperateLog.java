package com.putao.domain;

import com.putao.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysOperateLog extends BaseDomain implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 操作用户
     */
    private String username;

    /**
     * 操作内容
     */
    private String operation;

    /**
     * 耗时
     */
    private String time;

    /**
     * 操作方法
     */
    private String method;

    /**
     * 方法参数
     */
    private String params;

    /**
     * 操作者IP
     */
    private String ip;

    /**
     * 操作地点
     */
    private String location;

    /**
     * 接口调用结果
     */
    private String result;

    /**
     * 接口调用结果值
     */
    private String resultStr;
}
