package com.putao.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.putao.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUser extends BaseDomain implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 角色id
     */
    private String roleIds;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 手机号码
     */
    private String phone;


    /**
     * 账号状态（1：已登录，0：未登录）
     */
    private Integer loginStatus;

    /**
     * 头像url
     */
    private String avatar;

    /**
     * 码上使用微信的openId进行MD5加密后的id，一个微信用户一个此id
     */
    private String openId;

    /**
     * 连续记账天数
     */
    private Integer ConsecutiveBkDays;

    public void createInit(){
        this.setCreateTime(new Date());
        this.setModifyTime(new Date());
        this.setDelFlag(BaseDomain.DEL_FLAG_NORMAL);
    }

    /**
     * 角色名字
     */
    /** 以下为非数据库字段属性*/
    @TableField(exist = false)
    private String roleName;
    @TableField(exist=false)
    private String checkPassword;
    @TableField(exist=false)
    private String verifyCode;
    @TableField(exist=false)
    private String token;
    @TableField(exist=false)
    private List<SysRole> list = new ArrayList<>();
    /** 登录状态 */
    public static final Integer LOGIN_STATUS_LOGIN_ING = 1;//正在登录
    public static final Integer LOGIN_STATUS_LOGIN_OUT = 0; // 未登录


}
