package com.putao.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.putao.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysDictItem extends BaseDomain implements Serializable {

    private static final long serialVersionUID=1L;




    /**
     * 字典id
     */
    private String dictId;

    /**
     * 字典项文本
     */
    private String itemText;

    /**
     * 字典项值
     */
    private String itemValue;

    /**
     * 描述
     */
    private String description;

    /**
     * 排序
     */
    private Integer sortOrder;

    /** 以下为非数据库字段属性*/
    @TableField(exist = false)
    private String dictName;        // 字典类型名称
}
