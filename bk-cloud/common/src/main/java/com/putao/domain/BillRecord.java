package com.putao.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.putao.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author putao
 * @since 2022-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_bill_record")
public class BillRecord extends BaseDomain implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 用户id
     */
    private String userId;
    /**
     * 账单id
     */
    private String billId;
    /**
     * 分类id
     */
    private String categoryId;

    /**
     * 账户id
     */
    private String accountId;

    /**
     * 账本id
     */
    private String accountBookId;

    public void setValueByBillBeforeInsert(Bill bill) {
        this.userId = bill.getCreateId();
        this.billId = bill.getId();
        this.categoryId = bill.getCategoryId();
        this.accountId = bill.getAccountId();
        this.accountBookId = bill.getAccountBookId();
        this.insertInit();
    }
}
