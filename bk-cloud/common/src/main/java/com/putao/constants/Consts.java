package com.putao.constants;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface Consts {
    /**验证码有效时间*/
    public static final int VERIFY_CODE_VAI_TIME = 5;  //单位分
    /**token有效时间*/
    public static final int USER_INFO_TOKEN_VAI_TIME = 30;  //单位分

    /**操作记录的单位 毫秒*/
    public static final String OPERATION_TIME_UNIT = "ms";
}
