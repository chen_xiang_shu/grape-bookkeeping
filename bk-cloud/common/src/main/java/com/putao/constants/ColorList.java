package com.putao.constants;

import cn.hutool.core.util.ObjectUtil;
import com.putao.base.BaseColor;
import com.putao.enums.Color;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@NoArgsConstructor
@Component
public class ColorList {

    public static List<Color> colorList = new ArrayList<>();

    static {
        colorList.add(Color.one);
        colorList.add(Color.two);
        colorList.add(Color.three);
        colorList.add(Color.four);
        colorList.add(Color.five);
        colorList.add(Color.six);
        colorList.add(Color.seven);
        colorList.add(Color.eight);
        colorList.add(Color.nine);
        colorList.add(Color.ten);
        colorList.add(Color.eleven);
        colorList.add(Color.twelve);
        colorList.add(Color.thirteen);
        colorList.add(Color.fourteen);
        colorList.add(Color.fifteen);
        colorList.add(Color.sixteen);
        colorList.add(Color.seventeen);
        colorList.add(Color.eighteen);
        colorList.add(Color.nineteen);
        colorList.add(Color.twenty);
        colorList.add(Color.twentyOne);
        colorList.add(Color.twentyTwo);
        colorList.add(Color.twentyThree);
        colorList.add(Color.twentyFour);
        colorList.add(Color.twentyFive);
        colorList.add(Color.twentySix);
        colorList.add(Color.twentySeven);
        colorList.add(Color.twentyEight);
        colorList.add(Color.twentyNine);
        colorList.add(Color.thirty);
        colorList.add(Color.thirtyOne);
        colorList.add(Color.thirtyTwo);
        colorList.add(Color.thirtyThree);
        colorList.add(Color.thirtyFour);
        colorList.add(Color.thirtyFive);
        colorList.add(Color.thirtySix);
        colorList.add(Color.thirtySeven);
        colorList.add(Color.thirtyEight);
        colorList.add(Color.thirtyNine);
        colorList.add(Color.forty);
        colorList.add(Color.fortyOne);
        colorList.add(Color.fortyTwo);
        colorList.add(Color.fortyThree);
    }

    public static <T extends BaseColor> void setColor(T t, int colorIndex) {
        if (colorIndex <= -1) {
            return;
        }
        Color color = ColorList.colorList.get(colorIndex);
        if (ObjectUtil.isNotEmpty(color)) {
            t.setIconColor(color.getIconColor());
            t.setCircularColor(color.getCircularColor());
            t.setLightColor(color.getLightColor());
            t.setBgcColor(color.getDeepColor());
        }
    }

}
