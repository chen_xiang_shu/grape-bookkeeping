package com.putao.constants;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface Constants {

    String INSERT_VALUE_REPEAT = "插入操作时数据重复";
    String REFLECT_CHECK_EMPTY_COLUMN_ARRAY_EMPTY = "反射检查字段为空";

    /**************************************************************** 账本常量 ********************************************************************************/
    String CATEGORY_TYPE_PAY = "pay";
    String CATEGORY_TYPE_INCOME = "income";
    String BILL_TYPE_PAY = "pay";
    String BILL_TYPE_INCOME = "income";
    /**************************************************************** 账本常量 ********************************************************************************/

    /**
     * 一个字符串长度 中文42，英文30
     */
    int CHAR_LENGTH_OF_CHINESE = 42;
    int CHAR_LENGTH_OF_ENGLISH = 30;
    /**
     * ,
     */
    String NULL = "";
    /**
     * ,
     */
    String COMMA = ",";
    /**
     * :
     */
    String M = ":";
    /**
     * SPACE
     */
    String SPACE = " ";
    /**
     * .
     */
    String D = ".";
    /**
     * -
     */
    String H = "-";
    /**
     * id
     */
    String ID = "id";
    /**
     * success
     */
    String SUCCESS = "success";
    /**
     * entitys
     */
    String ENTITYS = "entitys";
    /**
     * ?
     */
    String QUESTION_MARK = "?";
    /**
     * &
     */
    String AND = "&";
    /**
     * success
     */
    String RETURN = "return";
    /**
     * OK
     */
    String OK = "ok";
    /**
     * data
     */
    String DATA = "data";
    /**
     * X
     */
    String DIAGONAL = "/";
    /**
     * 超时时间
     */
    int TIMEOUT = 100000;
    /**
     * -1
     */
    String NO_STR = "-1";
    /**
     * 0
     */
    String ZERO_STR = "0";
    /**
     * 1
     */
    String ONE_STR = "1";
    /**
     * 1
     */
    int INT_ONE = 1;
    /**
     * 2
     */
    String TWO_STR = "2";
    /**
     * 0
     */
    int ZERO_INT = 0;
    /**
     * 4
     */
    int FOUR = 4;
    /**
     * NULL
     */
    Object STR_NULL = null;


    /********************订单对比异常start*************************/
    /**
     * 正常
     */
    String NORMAL = "normal";
    /**
     * 异常
     */
    String ABNORMAL = "abnormal";
    /**
     * token 头
     */
    String TOKEN_HEADER = "Authorization";
    /**
     * get请求
     */
    String REQUEST_TYPE_GET = "get";
    /**
     * post请求
     */
    String REQUEST_TYPE_POST = "post";
    /**
     * 删除标记字段
     */
    String DEL_FLAG = "del_flag";
    /**
     * 插入操作
     */
    String OPERATION_TYPE_INSERT = "insert";
    /**
     * 插入操作
     */
    String OPERATION_TYPE_UPDATE = "update";
    /**
     * 插入操作
     */
    String OPERATION_TYPE_DELETE = "delete";
    /**
     * 拦截请求结果 成功
     */
    String OPERATION_RESULT_SUCCESS = "success";
    /**
     * 拦截请求结果 失败
     */
    String OPERATION_RESULT_FAIL = "fail";
}
