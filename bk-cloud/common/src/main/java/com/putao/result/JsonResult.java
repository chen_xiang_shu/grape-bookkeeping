package com.putao.result;

import com.putao.enums.ResultEnum;
import lombok.Data;
import lombok.ToString;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@ToString
public class JsonResult<T> {
    private boolean success;
    private int code;
    private String message;
    private T data;
    private Object other;

    public JsonResult(Integer code, String message, int test){
        this.code = code;
        this.message = message;
    }

    //public JsonResult(T data, Object other) {
    //    this(CODE_SUCCESS, MESSAGE_SUCCESS, data);
    //    this.other = other;
    //}

    public JsonResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public JsonResult(ResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.message = resultEnum.getMessage();
        this.success = resultEnum.getSuccess();
    }

    public JsonResult(ResultEnum resultEnum, T data, Object other) {
        this.code = resultEnum.getCode();
        this.message = resultEnum.getMessage();
        this.success = resultEnum.getSuccess();
        this.data = data;
        this.other = other;
    }

    public JsonResult(String message, ResultEnum resultEnum, T data, Object other) {
        this.code = resultEnum.getCode();
        this.message = message;
        this.success = resultEnum.getSuccess();
        this.data = data;
        this.other = other;
    }

    public JsonResult(boolean success, int code, String message, T data, Object other) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
        this.other = other;
    }

    public JsonResult() {
    }


    /**
     * 新增
     */
    public static <S>  JsonResult insertSuccess(S data, Object other) {
        return new JsonResult(ResultEnum.INSERT_SUCCESS, data, other);
    }

    /**
     * 修改
     */
    public static <S>  JsonResult updateSuccess(S data, Object other) {
        return new JsonResult(ResultEnum.UPDATE_SUCCESS, data, other);
    }
    public static <S>  JsonResult updateNotFound(S data, Object other) {
        return new JsonResult(ResultEnum.UPDATE_NOT_FOUND, data, other);
    }

    /**
     * 删除
     */
    public static <S>  JsonResult deleteSuccess(S data, Object other) {
        return new JsonResult(ResultEnum.DELETE_SUCCESS, data, other);
    }
    public static <S>  JsonResult deleteNotFound(S data, Object other) {
        return new JsonResult(ResultEnum.DELETE_NOT_FOUND, data, other);
    }
    public static <S> JsonResult deleteErrorByStatus(S data, Object other) {
        return new JsonResult(ResultEnum.DELETE_STATUS_ERROR, data, other);
    }

    /**
     * 查询
     */
    public static <T>  JsonResult selectSuccess(T data, Object other) {
        return new JsonResult(ResultEnum.SELECT_SUCCESS, data, other);
    }
    public static <T>  JsonResult selectNotFound(T data, Object other) {
        return new JsonResult(ResultEnum.SELECT_NOT_FOUND, data, other);
    }

    /**
     * 参数不满足
     */
    public static JsonResult argumentError(Object data, Object other) {
        return new JsonResult(ResultEnum.VERIFY_PARAMETER_ERROR, data, other);
    }

    /**
     * 参数不满足
     */
    public static JsonResult argumentError(String message, Object data, Object other) {
        return new JsonResult(message, ResultEnum.VERIFY_PARAMETER_ERROR, data, other);
    }

    /**
     * 操作成功
     */
    public static JsonResult operationSuccess(Object data, Object other) {
        return new JsonResult(ResultEnum.OPERATION_SUCCESS, data, other);
    }
    /**
     * 操作失败
     */
    public static JsonResult operationError(String message, Object data, Object other) {
        return new JsonResult(message, ResultEnum.OPERATION_ERROR, data, other);
    }

    /**
     * 系统错误
     */
    public static JsonResult systemError(Object data, Object other) {
        return new JsonResult(ResultEnum.SYSTEM_ERROR, data, other);
    }


    /**
     * 认证失败
     */
    public static JsonResult authenticationError(Object data, Object other) {
        return new JsonResult(ResultEnum.AUTHENTICATION_ERROR, data, other);
    }

    /**
     * 认证失败
     */
    public static JsonResult noPermission(Object data, Object other) {
        return new JsonResult(ResultEnum.AUTHENTICATION_NO_PERMISSION, data, other);
    }
    /**
     * token失效
     */
    public static JsonResult invalidToken(Object data, Object other) {
        return new JsonResult(ResultEnum.INVALID_TOKEN, data, other);
    }


    /**
     * 兜底数据
     */
    public  static JsonResult callback(Object data, Object other) {
        return new JsonResult(ResultEnum.CALL_BACK_DATA, data, other);
    }

    public static JsonResult resultSuccess(Object data, Object other) {
        return new JsonResult(ResultEnum.OPERATION_SUCCESS, data, other);
    }

    public static JsonResult logoutSuccess(Object data, Object other) {
        return new JsonResult(ResultEnum.LOGOUT_SUCCESS, data, other);
    }

    // 登录后是否存在该用户


    //// 刷新token成功
    //public static boolean SUCCESS_REFRESH_SUCCESS = true;
    //public static Integer CODE_REFRESH_SUCCESS = 10002;
    //public static String MESSAGE_REFRESH_SUCCESS = "刷新成功";
    //public static <T> JsonResult refreshSuccess(T data, Object other){
    //    return new JsonResult(SUCCESS_REFRESH_SUCCESS, CODE_REFRESH_SUCCESS, MESSAGE_REFRESH_SUCCESS, data, other);
    //}
    //// 刷新token失败
    //public static boolean SUCCESS_REFRESH_ERROR = false;
    //public static Integer CODE_REFRESH_ERROR = 10003;
    //public static String MESSAGE_REFRESH_ERROR = "刷新失败";
    //public static <T> JsonResult refreshError(T data, Object other){
    //    return new JsonResult(SUCCESS_REFRESH_ERROR, CODE_REFRESH_ERROR, MESSAGE_REFRESH_ERROR, data, other);
    //}
    //
    //// 操作成功
    //public static boolean SUCCESS_OPERATE_SUCCESS = false;
    //public static Integer CODE_OPERATE_SUCCESS = 10000;
    //public static String MESSAGE_OPERATE_SUCCESS = "操作成功";
    //public static <T> JsonResult operateSuccess(T data, Object other){
    //    return new JsonResult(SUCCESS_OPERATE_SUCCESS, CODE_OPERATE_SUCCESS, MESSAGE_OPERATE_SUCCESS, data, other);
    //}
    //
    //// 系统异常
    //
    //
    //// 参数不合法
    //public static boolean SUCCESS_PARAMS_ERROR = true;
    //public static Integer CODE_PARAMS_ERROR = 10004;
    //public static String MESSAGE_PARAMS_ERROR = "参数不合法";
    //public static <T> JsonResult argumentError(T data, Object other){
    //    return new JsonResult(SUCCESS_PARAMS_ERROR, CODE_PARAMS_ERROR, MESSAGE_PARAMS_ERROR, data, other);
    //}
    //
    //
    //
    //// 退出成功
    //public static boolean SUCCESS_LOGIN_OUT_SUCCESS = true;
    //public static Integer CODE_LOGIN_OUT_SUCCESS = 10001;
    //public static String MESSAGE_LOGIN_OUT_SUCCESS = "退出成功";
    //public static <T> JsonResult loginOutSuccess(T data, Object other){
    //    return new JsonResult(SUCCESS_LOGIN_OUT_SUCCESS, CODE_LOGIN_OUT_SUCCESS, MESSAGE_LOGIN_OUT_SUCCESS, data, other);
    //}
    //
    //
    //// 查询相关 （20000， 20001， 20002）
    //public static boolean SUCCESS_SELECT_SUCCESS = true;
    //public static Integer CODE_SELECT_SUCCESS = 20000;
    //public static String MESSAGE_SELECT_SUCCESS = "查询成功";
    //
    //public static boolean SUCCESS_SELECT_NOT_FOUND = false;
    //public static Integer CODE_SELECT_NOT_FOUND = 20001;
    //public static String MESSAGE_SELECT_NOT_FOUND = "找不到数据";
    //
    //public static <T> JsonResult selectSuccess(T data, Object other){
    //    return new JsonResult(SUCCESS_SELECT_SUCCESS, CODE_SELECT_SUCCESS, MESSAGE_SELECT_SUCCESS, data, other);
    //}
    //public static <T> JsonResult selectNotFound(T data, Object other){
    //    return new JsonResult(SUCCESS_SELECT_NOT_FOUND, CODE_SELECT_NOT_FOUND, MESSAGE_SELECT_NOT_FOUND, data, other);
    //}
    //
    //
    //
    //
    //// 新增相关
    //public static boolean SUCCESS_INSERT_SUCCESS = true;
    //public static Integer CODE_INSERT_SUCCESS = 30000;
    //public static String MESSAGE_INSERT_SUCCESS = "新增成功";
    //public static <T> JsonResult insertSuccess(T data, Object other){
    //    return new JsonResult(SUCCESS_INSERT_SUCCESS, CODE_INSERT_SUCCESS, MESSAGE_INSERT_SUCCESS, data, other);
    //}
    //
    //// 修改相关
    //public static boolean SUCCESS_UPDATE_SUCCESS = true;
    //public static Integer CODE_UPDATE_SUCCESS = 40000;
    //public static String MESSAGE_UPDATE_SUCCESS = "修改成功";
    //public static boolean SUCCESS_UPDATE_NOT_FOUND = false;
    //public static Integer CODE_UPDATE_NOT_FOUND = 40002;
    //public static String MESSAGE_UPDATE_NOT_FOUND = "找不到数据";
    //public static <T> JsonResult updateSuccess(T data, Object other){
    //    return new JsonResult(SUCCESS_UPDATE_SUCCESS, CODE_UPDATE_SUCCESS, MESSAGE_UPDATE_SUCCESS, data, other);
    //}
    //public static <T> JsonResult updateNotFound(T data, Object other){
    //    return new JsonResult(SUCCESS_UPDATE_NOT_FOUND, CODE_UPDATE_NOT_FOUND, MESSAGE_UPDATE_NOT_FOUND, data, other);
    //}
    //
    //// 删除相关
    //public static boolean SUCCESS_DELETE_SUCCESS = true;
    //public static Integer CODE_DELETE_SUCCESS = 60000;
    //public static String MESSAGE_DELETE_SUCCESS = "删除成功";
    //
    //public static boolean SUCCESS_DELETE_NOT_FOUND = false;
    //public static Integer CODE_DELETE_NOT_FOUND = 60001;
    //public static String MESSAGE_DELETE_NOT_FOUND = "找不到数据";
    //
    //public static boolean SUCCESS_DELETE_STATUS_ERROR = false;
    //public static Integer CODE_DELETE_STATUS_ERRORD = 60003;
    //public static String MESSAGE_DELETE_STATUS_ERROR = "删除失败，不符合删除条件";
    //
    //public static <T> JsonResult deleteSuccess(T data, Object other){
    //    return new JsonResult(SUCCESS_DELETE_SUCCESS, CODE_DELETE_SUCCESS, MESSAGE_DELETE_SUCCESS, data, other);
    //}
    //public static <T> JsonResult deleteNotFound(T data, Object other){
    //    return new JsonResult(SUCCESS_DELETE_NOT_FOUND, CODE_DELETE_NOT_FOUND, MESSAGE_DELETE_NOT_FOUND, data, other);
    //}
    //public static <T> JsonResult deleteErrorByStatus(T data, Object other){
    //    return new JsonResult(SUCCESS_DELETE_STATUS_ERROR, CODE_DELETE_STATUS_ERRORD, MESSAGE_DELETE_STATUS_ERROR, data, other);
    //}
    //
    //// 其他相关
    //
    //
    //
    //public static Integer CODE_BUSINESS = 111;
    //public static String MESSAGE_BUSINESS = "出错啦";
    //// 用于判断是否存在
    //public static Integer CODE_DATA_EXIST = 999;
    //public static String MESSAGE_DATA_EXIST = "数据已经存在";
    //
    //public static Integer CODE_SUCCESS = 20000;
    //public static String MESSAGE_SUCCESS = "操作成功";
    //public static Integer CODE_FAIL = 20001;
    //public static String MESSAGE_FAIL = "操作失败";
    //public static Integer GET_CODE_FAIL = 20002;
    //public static String GET_MESSAGE_FAIL = "参数缺少，请检查必填参数";
    //public static Integer SELECT_NOT_FOUND_CODE = 20003;
    //public static String SELECT_NOT_FOUND_MESSAGE = "找不到数据";
    //
    //// 系统异常
    //public static Integer CODE_SYSTEM_ERROR = 999;
    //public static String MESSAGE_SYSTEM_ERROR = "系统异常";
    //
    //public static <T> JsonResult success(T data, Object other){
    //    return new JsonResult(data, other);
    //}
    //
    //public static <T> JsonResult success(T data, Map<String, Object> other){
    //    return new JsonResult(data, other);
    //}
    //
    //public static <T> JsonResult success(){
    //    return new JsonResult(JsonResult.CODE_SUCCESS, JsonResult.MESSAGE_SUCCESS);
    //}
    //public static <T> JsonResult loginOut(){
    //    return new JsonResult(JsonResult.CODE_LOGIN_OUT_SUCCESS, JsonResult.MESSAGE_LOGIN_OUT_SUCCESS);
    //}
    //public static JsonResult getArgumentError() {
    //    return new JsonResult(JsonResult.GET_CODE_FAIL, GET_MESSAGE_FAIL, null);
    //}
    //public static JsonResult selectNotFound() {
    //    return new JsonResult(JsonResult.SELECT_NOT_FOUND_CODE, SELECT_NOT_FOUND_MESSAGE, null);
    //}
    //
    //public static JsonResult insertSuccess(Object obj) {
    //    return new JsonResult(JsonResult.CODE_INSERT_SUCCESS, MESSAGE_INSERT_SUCCESS, obj);
    //}
}
