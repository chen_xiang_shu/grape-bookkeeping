package com.putao.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
public class WebsocketResult implements Serializable {

    private boolean success;
    private String sign;
    private Object data;
    private String key;
    public static final String SIGN_TIME_OUT = "timeout";
    public static final String SIGN_LOGIN_SUCCESS = "loginSuccess";
    public static final String SIGN_SCAN_SUCCESS = "scanSuccess";
    public static final String SIGN_CANCEL_LOGIN_PC = "cancelLoginPc";

    public WebsocketResult(boolean success, String sign, Object data, String key) {
        this.success = success;
        this.sign = sign;
        this.data = data;
        this.key = key;
    }


    public static WebsocketResult timeOut(Object data, String key) {
        return new WebsocketResult(false, SIGN_TIME_OUT, data, key);
    }

    public static WebsocketResult loginSuccess(Object data, String key) {
        return new WebsocketResult(true, SIGN_LOGIN_SUCCESS, data, key);
    }

    public static WebsocketResult scanSuccess(Object data, String key) {
        return new WebsocketResult(true, SIGN_SCAN_SUCCESS, data, key);
    }
    public static WebsocketResult cancelLoginPc(Object data, String key) {
        return new WebsocketResult(true, SIGN_CANCEL_LOGIN_PC, data, key);
    }
}
