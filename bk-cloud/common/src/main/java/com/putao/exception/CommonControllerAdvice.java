package com.putao.exception;

import com.putao.error.BusinessException;
import com.putao.result.JsonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public class CommonControllerAdvice {


    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public JsonResult handleBusinessException(BusinessException ex){
        return JsonResult.systemError(ex.getMessage(), null);
    }


    @ExceptionHandler(Exception.class)
    @ResponseBody
    public JsonResult handleDefaultException(Exception ex){
        ex.printStackTrace();//在控制台打印错误消息.
        return JsonResult.systemError(ex.getMessage(), null);
    }
}
