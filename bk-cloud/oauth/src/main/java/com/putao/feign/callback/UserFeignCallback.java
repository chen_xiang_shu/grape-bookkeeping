package com.putao.feign.callback;

import com.putao.domain.SysOperateLog;
import com.putao.domain.SysUser;
import com.putao.feign.UserFeign;
import com.putao.result.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class UserFeignCallback implements UserFeign {

    @Override
    public JsonResult getByUsername(String username) {
        log.error("根据用户名查出用户..........");
        return JsonResult.callback("兜底数据", null);
    }

    @Override
    public JsonResult<SysUser> getByPhone(String phone) {
        log.error("根据手机号码查出用户....................出用户");
        return JsonResult.callback("兜底数据", null);
    }

    @Override
    public JsonResult getRolesByRoleIds(String roleIds) {
        log.error("根据角色id查出角色..........");
        return JsonResult.callback("兜底数据", null);
    }

    @Override
    public JsonResult getByUserId(String userId) {
        log.error("User Server Fallback..........");
        return JsonResult.callback("兜底数据", null);
    }

    @Override
    public JsonResult listByUserId(List<String> userIds) {
        log.error("User Server Fallback..........");
        return JsonResult.callback("兜底数据", null);
    }

    @Override
    public JsonResult getPermissionByUserId(String userId) {
        log.error("User Server Fallback..........");
        return JsonResult.callback("兜底数据", null);
    }

    @Override
    public JsonResult getByEmail(String email) {
        log.error("User Server Fallback..........");
        return JsonResult.callback("兜底数据", null);
    }

    @Override
    public JsonResult insert(SysOperateLog sysOperateLog) {
        log.error("User Server Fallback..........");
        return JsonResult.callback("兜底数据", null);
    }
}
