package com.putao.feign;

import com.putao.domain.SysOperateLog;
import com.putao.domain.SysPermission;
import com.putao.domain.SysRole;
import com.putao.domain.SysUser;
import com.putao.feign.callback.UserFeignCallback;
import com.putao.result.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
/**
 * user的feign接口
 */
@FeignClient(name = "system-service", fallback = UserFeignCallback.class)
public interface UserFeign {

    @GetMapping("/users/getByUsername")
    JsonResult<SysUser> getByUsername(@RequestParam(value = "username") String username);

    @GetMapping("/users/getByPhone")
    JsonResult<SysUser> getByPhone(@RequestParam(value = "phone") String phone);

    @GetMapping("/users/getRolesByRoleIds")
    JsonResult<List<SysRole>> getRolesByRoleIds(@RequestParam(value = "roleIds") String roleIds);

    @GetMapping("/users/getByUserId")
    JsonResult<SysUser> getByUserId(@RequestParam(value = "userId") String userId);

    @GetMapping("/users/listByUserId")
    JsonResult<List<SysUser>> listByUserId(@RequestBody List<String> userIds);

    @GetMapping("/users/getPermissionByUserId")
    JsonResult<List<SysPermission>> getPermissionByUserId(@RequestParam(value = "userId") String userId);

    @GetMapping("/users/getByEmail")
    JsonResult<SysUser> getByEmail(@RequestParam(value = "email") String email);

    @PostMapping("/sysOperateLogs")
    JsonResult insert(@RequestBody SysOperateLog sysOperateLog);

}
