package com.putao.feign;

import com.putao.feign.callback.WebsocketFeignCallback;
import com.putao.result.JsonResult;
import com.putao.result.WebsocketResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
/**
 * user的feign接口
 */
@FeignClient(name = "websocket-service", fallback = WebsocketFeignCallback.class)
public interface WebsocketFeign {

    @PostMapping("/messages")
    JsonResult sendMessage(@RequestBody WebsocketResult message);

}
