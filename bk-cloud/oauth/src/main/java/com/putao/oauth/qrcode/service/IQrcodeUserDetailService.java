package com.putao.oauth.qrcode.service;

import org.springframework.security.core.userdetails.UserDetails;

/*
 @Author:putao
 @Date:2022-06-28 20:20:27
*/
public interface IQrcodeUserDetailService {
    UserDetails loadUserByUserId(String phone, String code);
}
