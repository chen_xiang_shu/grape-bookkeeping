package com.putao.oauth.qrcode.provider;

import com.putao.oauth.code.service.IPhoneUserDetailService;
import com.putao.oauth.qrcode.service.IQrcodeUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.stereotype.Component;

/*
 @Author:putao
 @Date:2022-06-28 20:58:23
*/
@Component
public class QrcodeSecurityConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    /**
     *  第五步 将自定义的Provider注入IOC容器中
     *  必须要将对应的授权处理类注入到容器中，否则会报错，无法找到授权类
     *  定义一个类，继承 SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSercurity>
     *  重写configure方法，使用注入的方式注入根据邮箱查询用户信息的接口 和 密码解析器，创建对应的Provider子类，然后调用builder.authenticationProvider方法(参数)
     */

    @Autowired
    private IQrcodeUserDetailService qrcodeUserDetailService;

    @Override
    public void configure(HttpSecurity builder) throws Exception {
        QrcodeAuthenticationProvider qrcodeAuthenticationProvider = new QrcodeAuthenticationProvider(qrcodeUserDetailService);
        builder.authenticationProvider(qrcodeAuthenticationProvider);
    }
}
