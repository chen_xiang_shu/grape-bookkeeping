package com.putao.oauth.qrcode.provider;

import cn.hutool.core.util.ObjectUtil;
import com.putao.oauth.code.service.IPhoneUserDetailService;
import com.putao.oauth.qrcode.service.IQrcodeUserDetailService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/*
 @Author:putao
 @Date:2022-06-28 20:50:00
*/
public class QrcodeAuthenticationProvider implements AuthenticationProvider {

    /**
     * 第四步
     * 自定义AuthenticationProvider 处理类
     * 	这个类就是真正的处理类，经过TokenGranter后，会找到对应的AuthenticationProvider，
     * 	然后取出参数从数据库（UserDetailService）中查询对应的信息进行匹配，
     */

    private IQrcodeUserDetailService qrcodeUserDetailService;

    public QrcodeAuthenticationProvider(IQrcodeUserDetailService qrcodeUserDetailService) {
        this.qrcodeUserDetailService = qrcodeUserDetailService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // 处理逻辑
        QrcodeAuthenticationToken authenticationToken = (QrcodeAuthenticationToken) authentication;
        String key = (String)authentication.getPrincipal();
        String userId =(String) authentication.getCredentials();
        UserDetails userDetails = qrcodeUserDetailService.loadUserByUserId(key, userId);
        if (ObjectUtil.isEmpty(userDetails)) {
            throw new BadCredentialsException("key或者用户id错误");
        }
        QrcodeAuthenticationToken authenticationResult = new QrcodeAuthenticationToken(userDetails, userId, userDetails.getAuthorities());
        authenticationResult.setDetails(authenticationToken.getDetails());
        return authenticationResult;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        // No AuthenticationProvider found for
        return QrcodeAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
