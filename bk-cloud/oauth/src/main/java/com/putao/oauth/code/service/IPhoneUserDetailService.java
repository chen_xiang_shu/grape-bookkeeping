package com.putao.oauth.code.service;

import org.springframework.security.core.userdetails.UserDetails;

/*
 @Author:putao
 @Date:2022-06-28 20:20:27
*/
public interface IPhoneUserDetailService {
    UserDetails loadUserByPhone(String phone, String code);
}
