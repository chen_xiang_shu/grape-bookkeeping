package com.putao.exception;



import com.putao.result.JsonResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.jwt.crypto.sign.InvalidSignatureException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.UnsupportedGrantTypeException;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;

/**
 * 自定义异常翻译器，针对用户名、密码异常，授权类型不支持的异常进行处理
 */
@SuppressWarnings("ALL")
public class OAuthServerWebResponseExceptionTranslator implements WebResponseExceptionTranslator{
    /**
     * 业务处理方法，重写这个方法返回客户端信息
     */
    @Override
    public ResponseEntity<JsonResult> translate(Exception e){
        e.printStackTrace();
        JsonResult resultMsg = doTranslateHandler(e);
        return new ResponseEntity<>(resultMsg, HttpStatus.UNAUTHORIZED);
    }

    /**
     * 根据异常定制返回信息
     *  自己根据业务封装
     */
    private JsonResult doTranslateHandler(Exception e) {
        //AuthorizationEndpoint
        //初始值，系统错误，
        JsonResult resultCode;
        //判断异常，不支持的认证方式
        if (e instanceof UnsupportedGrantTypeException){
            resultCode = JsonResult.authenticationError("不支持的认证方式", null);
            //用户名或密码异常
        } else if(e instanceof InvalidGrantException){
            // 如果包含 authorization_code 说明是重复获取，返回错误信息是该code已过期，请重新验证授权
            if (e.getMessage().contains("Invalid authorization code")) {
                resultCode = JsonResult.authenticationError("code 已过期，请重新验证授权", null);
            } else {
                resultCode = JsonResult.authenticationError("用户名或者密码错误", null);
            }
        } else if (e instanceof InvalidTokenException) {
            resultCode = JsonResult.invalidToken(e.getMessage(), null);
        } else if (e instanceof InvalidSignatureException) {
            resultCode = JsonResult.invalidToken(e.getMessage(), null);
        }
        return JsonResult.systemError(e.getMessage(), null);
    }
}
