package com.putao.web.controller;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/*
 @Author:putao
 @Date:2022-06-25 18:08:19
*/
@Controller
public class PageController {

    @GetMapping("/oauth/loginPage")
    public String loginPage(@RequestParam Map<String, String> parameters, Map<String, Object> model1, Model model, AuthorizationRequest request) {
        //AuthorizationEndpoint
        //AuthorizationRequest authorizationRequest = (AuthorizationRequest) model1.get("authorizationRequest");
        String redirectUri = request.getRedirectUri();
        System.out.println("redirectUri = " + redirectUri);
        model.addAttribute("loginProcessUrl","/oauth/authorize");
        return "loginPage";
    }

    //@GetMapping("/oauth/errorBack")
    //public ModelAndView errors(Map<String, Object> model, HttpServletRequest request) {
    //    AuthorizationRequest authorizationRequest = (AuthorizationRequest) model.get("authorizationRequest");
    //    ModelAndView view = new ModelAndView();
    //    view.setViewName("client-error");
    //    view.addObject("clientId", authorizationRequest.getClientId());
    //    view.addObject("scopes",authorizationRequest.getScope());
    //    return view;
    //    //AuthorizationEndpoint
    //}

    public static void main(String[] args) {
        //PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String a = "$2a$10$iu40mk7aSAFubIrTxb08FuUQt9O3gtOqCeuDJFNYIEMA2ENWhFsfO";
        //"$2a$10$HWuOIx8C.YvlhLwp2j5LYe/r8B04xtcFmuu6t1XEBrnr2JLGFcc0q"
        //String encode = bCryptPasswordEncoder.encode("123");
        boolean result = bCryptPasswordEncoder.matches("123", "$2a$10$iu40mk7aSAFubIrTxb08FuUQt9O3gtOqCeuDJFNYIEMA2ENWhFsfO");
        System.out.println("result = " + result);
        //System.out.println("bCryptPasswordEncoder.encode(\"123456\") = " + bCryptPasswordEncoder.encode("123"));
    }
}
