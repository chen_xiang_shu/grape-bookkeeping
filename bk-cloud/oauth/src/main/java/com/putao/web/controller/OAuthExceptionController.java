package com.putao.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class OAuthExceptionController {

    private static final Logger logger = LoggerFactory.getLogger(OAuthExceptionController.class);

    @RequestMapping("/oauth/error")
    public ModelAndView defaultOAuthErrorHandler(Map<String, Object> model, HttpServletRequest request) throws Exception {
        logger.error("认证错误 Oauth2 exception occurred");
        //AuthorizationRequest authorizationRequest = (AuthorizationRequest) model.get("authorizationRequest");
        ModelAndView view = new ModelAndView();
        view.setViewName("client-error");
        //view.addObject("clientId", authorizationRequest.getClientId());
        //view.addObject("scopes",authorizationRequest.getScope());
        model.put("path", request.getServletPath());
        return view;
    }
}
