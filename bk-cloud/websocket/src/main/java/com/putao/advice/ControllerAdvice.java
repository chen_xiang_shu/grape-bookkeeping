package com.putao.advice;

import com.putao.exception.CommonControllerAdvice;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/*
 @Author:putao
 @Date:2022-07-11 10:23:21
*/
@RestControllerAdvice
public class ControllerAdvice extends CommonControllerAdvice {
}
