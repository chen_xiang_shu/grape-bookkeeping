package com.putao.server;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.putao.result.WebsocketResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint("/websocket/{key}")
@Component
@Slf4j
public class WebSocketServer {
    /**
     * 日志对象
     */
    /**
     * 订单页面连接Map 用户id+客户订单id -> webSocketServer
     */
    private static final ConcurrentHashMap<String, WebSocketServer> MAP = new ConcurrentHashMap<>();
    /**
     * 会话
     */
    private Session session;
    private Long date;

    /**
     * 连接建立成功调用的方法
     *
     * @param
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("key") String key) {
        this.session = session;
        this.date = new Date().getTime();
        log.info("用户:" + key + "连接成功");
        // 一个用户只能连接一个.根据用户id取出全部的连接然后移除掉
        if (!MAP.containsKey(key)) {
            MAP.put(key, this);
        }
        log.error("当前连接的总人数有{}, 分别是{}", MAP.size(), MAP);
    }


    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, @PathParam("key") String key) throws IOException {
        //检查页面 防止断开连接
//        if (CHECK.equals(message)) {
//            log.info("用户:" + key + "收到来自订单窗口:" + orderId + "的信息:" + message);
//            this.sendMessage(message);
//        }
//
//        // 发送给订单页面信息 刷新数据
//        if (REFRESH.equals(message)) {
//            log.info("用户:" + key + "收到来自订单窗口:" + orderId + "的信息:" + REFRESH);
//            sendMessageToOrder(orderId, key, message);
//        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(@PathParam("key") String key) {
        //断开连接
        MAP.remove(key);
    }


    /**
     * 发生错误时调用
     */
    @OnError
    public void onError(Session session, @PathParam("key") String key, Throwable error)
            throws IOException {
        log.info("WebSocket服务异常:" + error.getMessage());
        //关闭session
        if (session.isOpen()) {
            session.close();
        }
        //断开连接
        MAP.remove(key);
    }

    /**
     * 发送消息
     *
     * @param message 信息内容
     */
    public void sendMessage(String key, WebsocketResult message) throws IOException {
        WebSocketServer webSocketServer = MAP.get(key);
        if (ObjectUtil.isEmpty(webSocketServer)) {
            return;
        }
        if (webSocketServer.session.isOpen()) {
            log.info("发送信息:" + message);
            webSocketServer.session.getBasicRemote().sendText(JSON.toJSONString(message));
        } else {
            log.error("连接已经关闭，无法发送消息:" + message);
        }
        log.error("开始清除连接");
        long now = new Date().getTime();
        MAP.values().stream().filter(a -> now - a.date >= 60 * 1000 * 5).forEach(a -> {
            if (a.session.isOpen()) {
                try {
                    a.session.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

}
