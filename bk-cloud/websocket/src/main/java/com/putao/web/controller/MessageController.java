package com.putao.web.controller;

import cn.hutool.core.util.ObjectUtil;
import com.putao.base.BaseController;
import com.putao.base.BaseDomain;
import com.putao.result.JsonResult;
import com.putao.result.WebsocketResult;
import com.putao.server.WebSocketServer;
import com.putao.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/*
 @Author:putao
 @Date:2022-07-06 21:15:22
*/
@RestController
@RequestMapping("/messages")
@RefreshScope
@Slf4j
public class MessageController {

    @Autowired
    private WebSocketServer webSocketServer;

    @PostMapping
    public JsonResult sendMessage(@RequestBody WebsocketResult message) {
        if (ObjectUtil.isNotEmpty(message) && StringUtils.isNotBlank(message.getKey())) {
            try {
                webSocketServer.sendMessage(message.getKey(), message);
                return JsonResult.operationSuccess(message, null);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return JsonResult.argumentError(message, null);
    }
}
