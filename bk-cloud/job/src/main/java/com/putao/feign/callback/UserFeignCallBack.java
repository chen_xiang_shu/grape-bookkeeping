package com.putao.feign.callback;

import com.putao.domain.SysUser;
import com.putao.feign.UserFeign;
import com.putao.result.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 @Author:putao
 @Date:2022-07-17 10:02:02
*/
@Component
@Slf4j
public class UserFeignCallBack implements UserFeign {

    @Override
    public JsonResult increaseDays(List<SysUser> userList) {
        log.error("增加用户的记账天数..........");
        return JsonResult.callback(false, null);
    }

    @Override
    public JsonResult getUsers() {
        log.error("查询全部的用户..........");
        return JsonResult.callback("兜底数据", null);
    }

}
