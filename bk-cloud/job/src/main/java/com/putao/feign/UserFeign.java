package com.putao.feign;

import com.putao.domain.SysUser;
import com.putao.feign.callback.UserFeignCallBack;
import com.putao.result.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/*
 @Author:putao
 @Date:2022-07-17 10:01:08
*/
@FeignClient(name = "system-service", fallback = UserFeignCallBack.class)
public interface UserFeign {

    @PostMapping("/users/increaseDays")
    JsonResult increaseDays(@RequestBody List<SysUser> userList);


    @GetMapping("/users/getUsers")
    JsonResult<List<SysUser>> getUsers();

}
