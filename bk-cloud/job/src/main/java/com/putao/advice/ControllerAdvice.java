package com.putao.advice;

import com.putao.exception.CommonControllerAdvice;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/*
 @Author:putao
 @Date:2022-07-17 16:52:06
*/
@RestControllerAdvice
public class ControllerAdvice extends CommonControllerAdvice {
}
