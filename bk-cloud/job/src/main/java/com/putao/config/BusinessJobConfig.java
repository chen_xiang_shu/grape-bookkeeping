package com.putao.config;

import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.lite.spring.api.SpringJobScheduler;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.putao.job.IncreaseDayJob;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.putao.util.ElasticJobUtil;

@Configuration
public class BusinessJobConfig {

    @Bean(initMethod = "init")
    public SpringJobScheduler initIncreaseDayJob(CoordinatorRegistryCenter registryCenter, IncreaseDayJob increaseDayJob){
        LiteJobConfiguration jobConfiguration = ElasticJobUtil.createDefaultSimpleJobConfiguration(increaseDayJob.getClass(), increaseDayJob.getCron());
        return new SpringJobScheduler(increaseDayJob, registryCenter,jobConfiguration );
    }

}
