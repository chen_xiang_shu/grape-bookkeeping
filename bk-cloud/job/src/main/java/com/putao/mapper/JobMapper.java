package com.putao.mapper;

import com.putao.domain.Job;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author PuTao
 * @since 2022-07-17
 */
@Mapper
public interface JobMapper extends BaseMapper<Job> {

}
