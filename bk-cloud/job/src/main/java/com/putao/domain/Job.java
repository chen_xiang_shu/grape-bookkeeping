package com.putao.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.putao.base.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author PuTao
 * @since 2022-07-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_job")
public class Job extends BaseDomain implements Serializable {

    public static final String JOB_RESULT_SUCCESS = "success";              // 成功
    public static final String JOB_RESULT_FAIL = "fail";                    // 失败
    private static final long serialVersionUID=1L;

    /**
     * 运行表达式
     */
    private String cron;

    /**
     * 执行结果
     */
    private String result;

    /**
     * 任务描述
     */
    private String describes;

    /**
     * 花费时间
     */
    private String spendTime;

    @Override
    public String toString() {
        return "Job{" +
        "id=" + this.getId() +
        ", cron=" + cron +
        ", result=" + result +
        ", describe=" + describes +
        "}";
    }
}
