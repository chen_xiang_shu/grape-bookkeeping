package com.putao.service.impl;

import com.putao.domain.Job;
import com.putao.mapper.JobMapper;
import com.putao.service.JobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author PuTao
 * @since 2022-07-17
 */
@Service
public class JobServiceImpl extends ServiceImpl<JobMapper, Job> implements JobService {

}
