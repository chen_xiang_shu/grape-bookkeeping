package com.putao.service;

import com.putao.domain.Job;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author PuTao
 * @since 2022-07-17
 */
public interface JobService extends IService<Job> {

}
