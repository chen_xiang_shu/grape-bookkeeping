package com.putao.job;

import com.putao.domain.SysUser;
import com.putao.feign.UserFeign;
import com.putao.result.JsonResult;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.concurrent.Callable;

// 这是任务的抽象
@Data
@NoArgsConstructor
public class GetContentTask implements Callable<JsonResult> {

    private SysUser user;

    private UserFeign userFeign;

    public GetContentTask(SysUser user, UserFeign userFeign) {
        this.user = user;
        this.userFeign = userFeign;
    }

    public JsonResult call() throws Exception {
        ArrayList<SysUser> userList = new ArrayList<>();
        userList.add(this.user);
        userList.forEach(user -> user.setConsecutiveBkDays(user.getConsecutiveBkDays() + 1));
        userFeign.increaseDays(userList);
        return JsonResult.operationSuccess(user, userFeign);
    }
}
