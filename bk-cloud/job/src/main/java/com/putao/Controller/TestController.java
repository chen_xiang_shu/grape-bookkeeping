package com.putao.Controller;

import cn.hutool.core.util.IdUtil;
import com.putao.domain.Job;
import com.putao.domain.SysUser;
import com.putao.feign.UserFeign;
import com.putao.job.IncreaseDayJob;
import com.putao.result.JsonResult;
import com.putao.service.JobService;
import com.putao.utils.SnowflakeConfig;
import com.putao.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

/*
 @Author:putao
 @Date:2022-07-17 11:20:55
*/
@RestController
@RequestMapping("job")
public class TestController {

    @Autowired
    private UserFeign userFeign;

    @Autowired
    private SnowflakeConfig snowflakeConfig;

    @Autowired
    private JobService jobService;

    @Value("${jobCron.increaseDay}")
    private String cron;


    @GetMapping
    public JsonResult test() {
        Job job = new Job();
        job.insertInit(snowflakeConfig.snowflakeId());
        job.setDescribes("通过多线程批量修改用户的记账总天数");
        job.setCron(cron);
        // 查出所有的用户，根据个数分成几批。然后使用线程池去执行update方法，然后等待全部执行完后插入一条记录到定时任务表中
        JsonResult<List<SysUser>> userResult = userFeign.getUsers();
        if (userResult.isSuccess()) {
            List<CompletableFuture<JsonResult>> futureList = new ArrayList<>();
            List<SysUser> userList =  userResult.getData();
            userList.forEach(user -> user.setConsecutiveBkDays(user.getConsecutiveBkDays() + 1));
            List<List<SysUser>> newUserList = IncreaseDayJob.groupListByQuantity(userList, 10);
            for (int i = 0; i < newUserList.size(); i++) {
                int finalI = i;
                futureList.add(CompletableFuture.supplyAsync( () -> {
                    // 增加天数
                    List<SysUser> users = newUserList.get(finalI);
                    return userFeign.increaseDays(users);
                }));
            }
            CompletableFuture.allOf(futureList.toArray(new CompletableFuture[0])).join();
            for (CompletableFuture<JsonResult> future : futureList) {
                try {
                    JsonResult jsonResult = future.get();
                    if (!jsonResult.isSuccess()) {
                        job.setResult(Job.JOB_RESULT_FAIL);
                        break;
                    }
                } catch (InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
            if (StringUtils.isBlank(job.getResult())) {
                job.setResult(Job.JOB_RESULT_SUCCESS);
            }
            jobService.save(job);
        }
        return JsonResult.resultSuccess(null, null);
    }
}
