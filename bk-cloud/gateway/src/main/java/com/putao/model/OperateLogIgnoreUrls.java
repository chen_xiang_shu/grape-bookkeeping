package com.putao.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "operate")
@Data
public class OperateLogIgnoreUrls {

    private List<String> ignoreUrls;
}
