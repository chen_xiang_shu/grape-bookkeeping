package com.putao.model;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "oauth.ignore")
@Data
public class WhiteUrls {
    //private List<String> urls = Arrays.stream(new String[]{"/oauth-service/oauth/token", "/oauth-service/oauth/authorize", "/oauth-service/oauth/check_token"}).distinct().collect(Collectors.toList());
    private List<String> urls;

}
