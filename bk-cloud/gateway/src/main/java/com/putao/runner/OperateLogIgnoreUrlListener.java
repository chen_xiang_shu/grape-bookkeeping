package com.putao.runner;

import com.alibaba.fastjson.JSON;
import com.putao.enums.VerifyCodeEnum;
import com.putao.model.OperateLogIgnoreUrls;
import com.putao.redis.service.IRedisService;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * 启动容器后将配置文件中的忽略记录日志的url放入到redis中
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Component
@Slf4j
public class OperateLogIgnoreUrlListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private IRedisService redisService;
    @Autowired
    private OperateLogIgnoreUrls operateLogIgnoreUrls;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
//        if (null == contextRefreshedEvent.getApplicationContext().getParent()) {//保证只执行一次
//            log.info("执行将操作切面的路径放入redis中");
//            redisService.set(VerifyCodeEnum.OPERATE_LOG_IGNORE_URL.join(Strings.EMPTY), JSON.toJSONString(operateLogIgnoreUrls.getIgnoreUrls()));
//        }
        log.info("执行将操作切面的路径放入redis中");
        redisService.set(VerifyCodeEnum.OPERATE_LOG_IGNORE_URL.join(Strings.EMPTY), JSON.toJSONString(operateLogIgnoreUrls.getIgnoreUrls()));
    }
}
