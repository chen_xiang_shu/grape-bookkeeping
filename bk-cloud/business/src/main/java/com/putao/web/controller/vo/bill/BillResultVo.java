package com.putao.web.controller.vo.bill;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BillResultVo {
    // 每月的数据
    private Map<String, BillOtherVo> yearBillOtherVoMap;
    private BigDecimal payMoney;
    private BigDecimal incomeMoney;
    private BigDecimal balances;
}
