package com.putao.web.controller.vo.bill;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BillOtherVo {
    private String month;
    private BigDecimal pay;
    private BigDecimal income;
    private String year;
    // 一个月中每天的汇总的数据
    private List<BillDataVo> billDataVoList;

    //private Map<String, BillDataVo> monthBillDataVoMap;
}
