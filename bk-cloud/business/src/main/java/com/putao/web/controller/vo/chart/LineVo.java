package com.putao.web.controller.vo.chart;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LineVo {
    private String name;
    private String type = "line";
    private String style = "curve";
    private boolean addPoint = true;
    private String fontColor = "#42cf4a";
    private String color;
    private String unit;
    private int fontSize = 2;
    private List<BigDecimal> data;
    private String day;


    public void getColorByName() {
        this.color = "支出".equals(name) ? "#cf6d6b" : "#41d087";
    }
}
