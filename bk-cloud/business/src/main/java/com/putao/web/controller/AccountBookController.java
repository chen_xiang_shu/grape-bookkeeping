package com.putao.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.putao.annotation.OperateMethodName;
import com.putao.base.BaseController;
import com.putao.domain.AccountBook;
import com.putao.other.AccountType;
import com.putao.result.JsonResult;
import com.putao.service.AccountBookService;
import com.putao.service.CategoryService;
import com.putao.utils.RequestUtils;
import com.putao.utils.StringUtils;
import com.putao.utils.ThreadLocalUtil;
import com.putao.vo.AccountBookVo;
import com.putao.vo.BaseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Api(tags = {"10.0 账本增删改查类"})
@RestController
@RequestMapping("/accountBooks")
@Slf4j
public class AccountBookController extends BaseController<AccountBook> {

    @Autowired
    private AccountBookService accountBookService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private List<AccountType> accountTypeList;

    /**
     * 账本列表
     *
     * @return
     */
    @GetMapping
//    @RequiresRoles(value={"admin"})
    @ApiOperation(value = "账本列表", notes = "base对象")
    @OperateMethodName(value = "账本列表", method = "accountBook:list")
    public JsonResult list(HttpServletRequest request, BaseVo baseVo) {
        // 如果是移动端，则查询出该用户的全部账本（新增时已经限制最多五个账本）
        String env = RequestUtils.getEnv(request);
        String userId = ThreadLocalUtil.getUserId();
        if (BaseVo.ENV_PHONE.equals(env)) {
            return getAccountBookListForPhone(baseVo, userId);
        }
        // 如果是PC端，则分页查询
        PageHelper.startPage(baseVo.getPageIndex(), baseVo.getPageSize());
        QueryWrapper<AccountBook> wrapper = createWrapper();
        wrapper.like(Strings.isNotBlank(baseVo.getSearch()), "name", baseVo.getSearch());
        wrapper.eq(StringUtils.isNotBlank(baseVo.getColumnName())
                && StringUtils.isNotBlank(baseVo.getSelect()), baseVo.getColumnName(), baseVo.getSelect());
        orderByModifyTimeDesc(wrapper);
        List<AccountBook> accountBookList = accountBookService.list(wrapper);
        return selectNotFound(accountBookList, accountBookList.size());
    }

    /**
     * 移动端返回账本列表 包含分类
     * @param baseVo
     * @param userId
     * @return
     */
    private JsonResult getAccountBookListForPhone(BaseVo baseVo, String userId) {
        QueryWrapper<AccountBook> wrapper = createWrapperWithUserId(userId);
        List<AccountBookVo> accountBookVoList = accountBookService.selectListByPhone(baseVo, userId, wrapper);
        return JsonResult.selectSuccess(accountBookVoList, accountBookVoList.size());
    }


    @PutMapping
    @ApiOperation(value = "账本修改", notes = "路径参数传入账本对象")
    @OperateMethodName(value = "账本修改", method = "accountBook:update")
    public JsonResult update(@RequestBody AccountBookVo accountBookVO){
        if (!Strings.isNotBlank(accountBookVO.getId())){
            return updateNotFound(accountBookVO, null);
        }
        AccountBook accountBook = new AccountBook();
        BeanUtils.copyProperties(accountBookVO, accountBook);
        accountBookService.updateAccountBook(accountBook);
        return updateSuccess(accountBook, null);
    }


    @PostMapping
    @ApiOperation(value = "添加账本", notes = "")
    @OperateMethodName(value = "添加账本", method = "accountBook:insert")
    public JsonResult insert(@RequestBody AccountBookVo accountBookVO){
        AccountBook accountBook = accountBookService.saveAccountBook(accountBookVO);
        return insertSuccess(accountBook, accountBookVO);
    }

    @DeleteMapping
    @ApiOperation(value = "删除", notes = "参数传入id")
    @OperateMethodName(value = "账本删除,批量", method = "accountBook:delete")
    public JsonResult delete(String id){
        return this.delete(id, accountBookService, categoryService, "account_book_id");
    }
}
