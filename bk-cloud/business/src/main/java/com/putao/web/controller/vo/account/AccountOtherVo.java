package com.putao.web.controller.vo.account;

import com.putao.vo.AccountVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountOtherVo {
    private String type;
    private String name;
    private BigDecimal money;
    private List<AccountVo> accountVoList = new ArrayList<>();
}
