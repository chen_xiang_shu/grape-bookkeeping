package com.putao.web.controller;

import com.putao.annotation.OperateMethodName;
import com.putao.base.BaseController;
import com.putao.base.BaseWrapper;
import com.putao.domain.Bill;
import com.putao.domain.BillRecord;
import com.putao.feign.SystemFeign;
import com.putao.result.JsonResult;
import com.putao.service.BillRecordService;
import com.putao.service.BillService;
import com.putao.utils.RequestUtils;
import com.putao.utils.ThreadLocalUtil;
import com.putao.vo.BaseVo;
import com.putao.vo.BillRecordVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Api(tags = {"13.0 账单记录增删改查类"})
@RestController
@RequestMapping("/billRecords")
@Slf4j
public class BillRecordController extends BaseController<BillRecord> {

    @Autowired
    private BillRecordService billRecordService;
    @Autowired
    private BillService billService;
    @Autowired
    private SystemFeign systemFeign;

    @GetMapping("getDay")
    @ApiOperation(value = "查询用户连续记账天数和总天数", notes = "base对象")
    @OperateMethodName(value = "查询用户连续记账天数和总天数", method = "billRecord:getDay")
    public JsonResult getDay(HttpServletRequest request, BaseVo baseVo) {
        String env = RequestUtils.getEnv(request);
        String userId = ThreadLocalUtil.getUserId();
        if (BaseVo.ENV_PHONE.equals(env)) {
            Integer continuityDay = billRecordService.selectDaysByUserId(userId);
            Integer totalDay = billService.count(new BaseWrapper<Bill>().init(userId));
            BillRecordVo billRecordVo = new BillRecordVo();
            billRecordVo.setTotalBills(totalDay);
            billRecordVo.setContinuityDay(continuityDay);
            JsonResult result = systemFeign.getDay();
            if (!result.isSuccess()) {
                return JsonResult.operationError("查询系统服务的用户天数失败", null, null);
            }
            billRecordVo.setTotalDay(Integer.valueOf(result.getData() + Strings.EMPTY));
            return JsonResult.selectSuccess(billRecordVo, null);
        }
        return selectNotFound(new ArrayList<>(), 0);
    }
}
