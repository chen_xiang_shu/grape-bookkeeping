package com.putao.web.controller.vo.chart;

import com.putao.constants.Constants;
import com.putao.domain.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChartVo {
    private Set<Integer> lineXSet;
    /** 折线图 */
    private List<LineVo> lineVoList;
    /** 饼图 */
    private List<PieVo> pieVoList;
    /** 横向柱形图 */
    private List<BarVo> barVoList;
    /** 预算 */
    private List<Category> categoryList;

    public static final List<String> typeList = new ArrayList<>();
    public static final List<String> ignoreColumnList = new ArrayList<>();
    static {
        typeList.add(Constants.BILL_TYPE_PAY);
        typeList.add(Constants.BILL_TYPE_INCOME);

        ignoreColumnList.add("typeList");
        ignoreColumnList.add("ignoreColumnList");
    }


}
