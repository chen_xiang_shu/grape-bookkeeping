package com.putao.web.controller.vo.bill;

import com.putao.domain.Bill;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BillDataVo {
    // 按照天分组
    private String dayTimeStr;
    // 存放每一天的账单集合，可能为0
    private List<Bill> billList;
    // 周几
    private String dayForWeek;
    // 支出
    private BigDecimal payMoney;
    // 收入
    private BigDecimal incomeMoney;
}
