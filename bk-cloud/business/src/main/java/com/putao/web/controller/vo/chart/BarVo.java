package com.putao.web.controller.vo.chart;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BarVo {
    private String name;
    private BigDecimal num;
    private String width = "";
    private String background;
    // 前端根据num和totalNum，得到比例后根据比例展示长度
    private BigDecimal totalNum;
    private String ratio;
}
