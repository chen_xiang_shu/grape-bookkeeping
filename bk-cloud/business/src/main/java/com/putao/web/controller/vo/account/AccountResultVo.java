package com.putao.web.controller.vo.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountResultVo {
    // 净资产
    private BigDecimal netAssets;
    // 总资产
    private BigDecimal totalAssets;
    // 总负债
    private BigDecimal totalLiabilities;
    private List<AccountOtherVo> accountOtherVoList = new ArrayList<>();
}
