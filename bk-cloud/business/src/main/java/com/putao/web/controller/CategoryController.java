package com.putao.web.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.putao.annotation.OperateMethodName;
import com.putao.base.BaseController;
import com.putao.base.BaseDomain;
import com.putao.domain.Bill;
import com.putao.domain.Category;
import com.putao.result.JsonResult;
import com.putao.service.CategoryService;
import com.putao.utils.RequestUtils;
import com.putao.utils.StringUtils;
import com.putao.utils.ThreadLocalUtil;
import com.putao.vo.BaseVo;
import com.putao.vo.BillVo;
import com.putao.vo.CategoryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Api(tags = {"11.0 分类增删改查类"})
@RestController
@RequestMapping("/categorys")
@Slf4j
public class CategoryController extends BaseController<Category> {

    @Autowired
    private CategoryService categoryService;

    /**
     * 分类列表
     *
     * @return
     */
    @GetMapping
//    @RequiresRoles(value={"admin"})
    @ApiOperation(value = "分类列表", notes = "base对象")
    @OperateMethodName(value = "分类列表", method = "category:list")
    public JsonResult list(HttpServletRequest request, BaseVo baseVo) {
        String env = RequestUtils.getEnv(request);
        String userId = ThreadLocalUtil.getUserId();
        if (BaseVo.ENV_PHONE.equals(env)) {
            return getCategoryListForPhone(baseVo, userId);
        }
        PageHelper.startPage(baseVo.getPageIndex(), baseVo.getPageSize());
        QueryWrapper<Category> wrapper = createWrapper();
        wrapper.like(Strings.isNotBlank(baseVo.getSearch()), "name", baseVo.getSearch());
        wrapper.eq(StringUtils.isNotBlank(baseVo.getColumnName())
                && StringUtils.isNotBlank(baseVo.getSelect()), baseVo.getColumnName(), baseVo.getSelect());
        orderByModifyTimeDesc(wrapper);
        List<Category> list = categoryService.list(wrapper);
        return selectNotFound(new ArrayList<>(), 0);
    }


    private JsonResult getCategoryListForPhone(BaseVo baseVo, String userId) {
        QueryWrapper<Category> wrapper = createWrapperWithUserId(userId);
        List<CategoryVo> categoryVoList = categoryService.selectListByPhone(baseVo, userId, wrapper);
        if (CollectionUtil.isNotEmpty(categoryVoList)) {
            return JsonResult.selectSuccess(categoryVoList, categoryVoList.size());
        }
        return JsonResult.selectNotFound(categoryVoList, 0);
    }

    @PutMapping
    @ApiOperation(value = "分类修改", notes = "路径参数传入分类对象")
    @OperateMethodName(value = "分类修改", method = "category:update")
    public JsonResult update(@RequestBody CategoryVo categoryVo){
        if (!Strings.isNotBlank(categoryVo.getId())){
            return updateNotFound(categoryVo, null);
        }
        Category category = new Category();
        BeanUtils.copyProperties(categoryVo, category);
        boolean result = categoryService.updateCategory(category);
        return updateSuccess(category, null);
    }


    @PostMapping
    @ApiOperation(value = "添加分类", notes = "")
    @OperateMethodName(value = "添加分类", method = "category:insert")
    public JsonResult insert(@RequestBody CategoryVo categoryVo){
        Category category = categoryService.saveCategory(categoryVo);
        return insertSuccess(category, categoryVo);
    }

    @DeleteMapping
    @ApiOperation(value = "删除", notes = "参数传入id")
    @OperateMethodName(value = "分类删除,批量", method = "category:delete")
    public JsonResult delete(@RequestParam String id){
        if (!Strings.isNotBlank(id)){
            return argumentError(id, null);
        }
        List<String> idList = StringUtils.convertList(id);
        boolean result = false;
        if (CollectionUtil.isNotEmpty(idList)) {
            Collection<Category> categoryList = categoryService.listByIds(idList);
            if (CollectionUtil.isNotEmpty(categoryList)) {
                for (Category category : categoryList) {
                    if (BaseDomain.DEL_FLAG_DELETE.equals(category.getDelFlag())) {
                        return deleteErrorByStatus(id, null);
                    }
                    category.setDelFlag(BaseDomain.DEL_FLAG_DELETE);
                }
                result = categoryService.updateBatchById(categoryList);
            }
        }
        return result ? deleteSuccess(id, null) : deleteNotFound(id, null);
    }
}
