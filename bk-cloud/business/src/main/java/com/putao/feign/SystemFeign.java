package com.putao.feign;

import com.putao.domain.SysOperateLog;
import com.putao.feign.callback.SystemFeignCallback;
import com.putao.result.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/*
 @Author:putao
 @Date:2022-07-11 15:05:35
*/
/**
 * user的feign接口
 */
@FeignClient(name = "system-service", fallback = SystemFeignCallback.class)
public interface SystemFeign {

    @GetMapping("/users/getDay")
    JsonResult getDay();


    @PostMapping("/sysOperateLogs")
    JsonResult insert(@RequestBody SysOperateLog sysOperateLog);

}
