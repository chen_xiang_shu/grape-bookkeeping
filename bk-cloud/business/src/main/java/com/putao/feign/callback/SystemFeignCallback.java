package com.putao.feign.callback;

import com.putao.domain.SysOperateLog;
import com.putao.feign.SystemFeign;
import com.putao.result.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SystemFeignCallback implements SystemFeign {

    @Override
    public JsonResult getDay() {
        log.error("系统服务 Server Fallback..........");
        return JsonResult.callback("兜底数据", null);
    }

    @Override
    public JsonResult insert(SysOperateLog sysOperateLog) {
        log.error("系统服务 Server Fallback..........");
        return JsonResult.callback("兜底数据", null);
    }
}
