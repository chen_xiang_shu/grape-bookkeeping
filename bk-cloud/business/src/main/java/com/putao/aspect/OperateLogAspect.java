package com.putao.aspect;

import com.alibaba.fastjson.JSON;
import com.putao.enums.VerifyCodeEnum;
import com.putao.feign.SystemFeign;
import com.putao.redis.service.IRedisService;
import org.apache.logging.log4j.util.Strings;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Aspect
@Component
public class OperateLogAspect extends CommonAspect {

//    @Value("${ignore.controllers}")
//    private String ignoreControllerString;
    @Autowired
    private SystemFeign systemFeign;
    @Autowired
    private IRedisService redisService;

    //定义切点
    @Pointcut(value = "execution(* com.putao.web.controller.*.*(..))")
    public void aopWebLog() {
    }
    //使用环绕通知
    @Around("aopWebLog()")
    public Object myLogger(ProceedingJoinPoint pjp) throws Throwable {
        String s = redisService.get(VerifyCodeEnum.OPERATE_LOG_IGNORE_URL.join(Strings.EMPTY));
        List<String> ignoreUrls = JSON.parseArray(s, String.class);
        return this.myLogger(pjp, sysOperateLog -> systemFeign.insert(sysOperateLog), ignoreUrls);
    }
}
