package com.putao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.Bill;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface BillMapper extends BaseMapper<Bill> {

}
