package com.putao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.BillRecord;
import org.apache.ibatis.annotations.Param;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface BillRecordMapper extends BaseMapper<BillRecord> {

    Integer selectDaysByUserId(@Param("userId") String userId);
}
