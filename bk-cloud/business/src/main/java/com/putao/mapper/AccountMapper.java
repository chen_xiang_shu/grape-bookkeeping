package com.putao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.Account;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface AccountMapper extends BaseMapper<Account> {

}
