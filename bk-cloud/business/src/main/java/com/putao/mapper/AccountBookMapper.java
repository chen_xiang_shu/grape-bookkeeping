package com.putao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.putao.domain.AccountBook;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface AccountBookMapper extends BaseMapper<AccountBook> {

}
