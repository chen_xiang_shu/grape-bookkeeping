//package com.putao.config;
//
//import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
//import com.google.common.collect.Lists;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.service.*;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spi.service.contexts.SecurityContext;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
//import java.util.List;
//
//@EnableSwagger2
//@Configuration
//@EnableKnife4j
////@Import(BeanValidatorPluginsConfiguration.class)
//public class Knife4jConfiguration {
//
//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .title("记账管理")
//                .description("记账管理接口说明文档")
//                .termsOfServiceUrl("")
//                .contact(new Contact("符和煌","2628279194@qq.com","2628279194@qq.com"))
//                .version("1.0")
//                .build();
//    }
//
//
//    @Bean
//    public Docket api() {
//        return new Docket(DocumentationType.SWAGGER_2).
//                useDefaultResponseMessages(false)
//                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.putao.web.controller"))
//                //regex("/api/.*")
//                .paths(PathSelectors.any())
//                .build()
//                .securitySchemes(securitySchemes())
//                .securityContexts(securityContexts())
////	                .globalOperationParameters(pars)
//                .apiInfo(apiInfo());
//    }
//
//    private List<ApiKey> securitySchemes() {
//        List<ApiKey> list = Lists.newArrayList();
//        //当前用户的访问令牌
//        list.add(new ApiKey("Authorization","Authorization",  "header"));
//        return list;
//    }
//
//    private AuthorizationScope[] scopes() {
//        AuthorizationScope[] scopes = {
//                new AuthorizationScope("read", "for read operations"),
//                new AuthorizationScope("write", "for write operations"),
//                new AuthorizationScope("foo", "Access foo API") };
//        return scopes;
//    }
//    private List<SecurityContext> securityContexts() {
//        return Lists.newArrayList(
//                SecurityContext.builder()
//                        .securityReferences(Lists.newArrayList(new SecurityReference("Authorization", scopes())))
//                        .forPaths(PathSelectors.any())
//                        .build()
//        );
//    }
//
////    @Bean(value = "defaultApi2")
////    public Docket defaultApi2() {
////        Docket docket=new Docket(DocumentationType.SWAGGER_2)
////                .apiInfo(new ApiInfoBuilder()
////                        //.title("swagger-bootstrap-ui-demo RESTful APIs")
////                        .description("# swagger-bootstrap-ui-demo RESTful APIs")
////                        .termsOfServiceUrl("http://www.xx.com/")
////                        .contact(new Contact("name","url","email"))
////                        .version("1.0")
////                        .build())
////                //配置token 或 请求头 Authorization
////                .securitySchemes(securitySchemes())
////                //分组名称
////                .groupName("2.X版本")
////                .select()
////                //只显示添加注解的方法
////                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
////                //这里指定Controller扫描包路径
////                .apis(RequestHandlerSelectors.basePackage("com.putao.controller"))
////                .paths(PathSelectors.any())
////                .build();
////        return docket;
////    }
////    //配置请求头的方法
////    private List<SecurityScheme> securitySchemes()
////    {
////        List<SecurityScheme> apiKeyList = new ArrayList<SecurityScheme>();
////      apiKeyList.add(new ApiKey("Authorization", "Authorization", In.HEADER.toValue()));
////        return apiKeyList;
////    }
//}
