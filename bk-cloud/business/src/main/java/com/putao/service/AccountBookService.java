package com.putao.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.AccountBook;
import com.putao.vo.AccountBookVo;
import com.putao.vo.BaseVo;

import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface AccountBookService extends IService<AccountBook> {

    /**
     * 新增账本
     * @param accountBookVO
     * @return
     */
    AccountBook saveAccountBook(AccountBookVo accountBookVO);

    /**
     * 查询移动端的账本集合，包含分类数据
     * @param baseVo
     * @param userId
     * @param wrapper
     * @return
     */
    List<AccountBookVo> selectListByPhone(BaseVo baseVo, String userId, QueryWrapper<AccountBook> wrapper);

    /**
     * 修改账本
     * @param accountBook
     * @return
     */
    void updateAccountBook(AccountBook accountBook);

    /**
     * 获取首个账本数据，根据创建时间升序
     * @return
     */
    AccountBook getAccountBookLimitOne();
}
