package com.putao.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.Account;
import com.putao.vo.AccountVo;
import com.putao.vo.BaseVo;

import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface AccountService extends IService<Account> {

    /**
     * 新增账户
     * @param accountVO
     * @return
     */
    Account saveAccount(AccountVo accountVO);

    /**
     * 查询用户的账单列表
     * @param baseVo
     * @param userId
     * @param wrapper
     * @return
     */
    List<AccountVo> selectListByPhone(BaseVo baseVo, String userId, QueryWrapper<Account> wrapper);

    /**
     * 修改账户信息
     * @param account 账户
     */
    void updateAccount(AccountVo accountVo);

    /**
     * 账单发生变化后修改分类的在金额
     *
     * @param billList  账单集合
     * @param accountId
     * @param userId
     */
    void updateMoney(String userId);

}
