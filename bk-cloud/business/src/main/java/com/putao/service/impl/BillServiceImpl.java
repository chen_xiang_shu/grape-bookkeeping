package com.putao.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.putao.base.BaseService;
import com.putao.base.BaseWrapper;
import com.putao.domain.Bill;
import com.putao.domain.BillRecord;
import com.putao.mapper.BillMapper;
import com.putao.service.AccountService;
import com.putao.service.BillRecordService;
import com.putao.service.BillService;
import com.putao.service.CategoryService;
import com.putao.utils.AssertUtil;
import com.putao.utils.ThreadLocalUtil;
import com.putao.vo.BaseVo;
import com.putao.vo.BillVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Service
public class BillServiceImpl extends ServiceImpl<BillMapper, Bill> implements BillService, BaseService<Bill> {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private BillRecordService billRecordService;

    @Override
    public List<Bill> selectListByPhone(BaseVo baseVo, String userId, QueryWrapper<Bill> wrapper) {
        wrapper.orderByDesc("create_time");
        List<Bill> billList = this.list(wrapper);
        if (CollectionUtil.isEmpty(billList)) {
            return new ArrayList<>();
        }
        return billList;
    }

    @Override
    public Bill saveBill(BillVo billVo) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(billVo, bill);
        bill.insertInit();
        bill.initProperties();
        this.check(bill, null);
        this.save(bill);
        this.afterUpdateBill(billVo);
        BillRecord billRecord = new BillRecord();
        billRecord.setValueByBillBeforeInsert(bill);
        billRecordService.save(billRecord);
        return bill;
    }

    /**
     * 账单数据发生变化后修改分类和账户的金额
     *
     * @param bill
     */
    public void afterUpdateBill(Bill bill) {
        String userId = ThreadLocalUtil.getUserId();
        List<Bill> categoryBillList = this.list(new BaseWrapper<Bill>().init(userId).eq("category_id", bill.getCategoryId()));
        accountService.updateMoney(userId);
        categoryService.updateMoney(userId);
    }

    @Override
    public List<Bill> getBillByAccountBookIdAndYearAndMonth(String accountBookId, String year, String month) {
        QueryWrapper<Bill> qw = new BaseWrapper<Bill>().init(ThreadLocalUtil.getUserId());
        qw.eq("account_book_id", accountBookId).eq("year_str", year).eq("month_str", month);
        return this.list(qw);
    }

    @Override
    public Bill updateBill(BillVo billVo) {
        Bill oldBill = this.getById(billVo.getId());
        Bill bill = new Bill();
        BeanUtils.copyProperties(billVo, bill);
        bill.updateInit();
        this.check(bill, billVo.getId());
        this.updateById(bill);
        this.afterUpdateBill(bill);
        return null;
    }

    private void check(Bill bill, String id) {
        // 检查月份，年份，与选择的时间是否一致
        AssertUtil.hasText(bill.getAccountId(), "账户不能为空");
        AssertUtil.hasText(bill.getCategoryId(), "分类不能为空");
        AssertUtil.hasText(bill.getYearStr(), "年份不能为空");
        AssertUtil.hasText(bill.getMonthStr(), "月份不能为空");
        AssertUtil.hasText(bill.getType(), "类型不能为空");
        AssertUtil.hasText(bill.getMoney(), "金额不能为空");
        AssertUtil.hasText(bill.getTime(), "时间不能为空");
        bill.checkYearAndMonth();
    }
}
