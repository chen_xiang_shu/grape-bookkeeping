package com.putao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.BillRecord;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface BillRecordService extends IService<BillRecord> {

    Integer selectDaysByUserId(String userId);
}
