package com.putao.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.Category;
import com.putao.vo.BaseVo;
import com.putao.vo.CategoryVo;

import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface CategoryService extends IService<Category> {

    /**
     * 新增分类
     * @param categoryVo
     * @return
     */
    Category saveCategory(CategoryVo categoryVo);

    /**
     * 修改分类
     * @param category
     * @return
     */
    boolean updateCategory(Category category);

    /**
     * 查询用户下所有的分类
     * @param baseVo
     * @param userId
     * @param wrapper
     * @return
     */
    List<CategoryVo> selectListByPhone(BaseVo baseVo, String userId, QueryWrapper<Category> wrapper);

    /**
     * 账单发生变化后修改分类的在金额
     *
     * @param billList   账单集合
     * @param categoryId
     */
    void updateMoney(String userId);

    /**
     * 根据账本id查出支出类型的分类
     * @param accountBookId 账本id
     * @return 支出类型的分类集合
     */
    List<Category> getForPayTypeByAccountBookId(String accountBookId);
}
