package com.putao.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.putao.domain.Bill;
import com.putao.vo.BaseVo;
import com.putao.vo.BillVo;

import java.util.List;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
public interface BillService extends IService<Bill> {

    /**
     * 查出用户所有的账单
     * @param baseVo
     * @param userId
     * @param wrapper
     * @return
     */
    List<Bill> selectListByPhone(BaseVo baseVo, String userId, QueryWrapper<Bill> wrapper);

    /**
     * 保存账单
     * @param billVO
     * @return
     */
    Bill saveBill(BillVo billVO);

    /**
     * 修改账单
     * @param billVo
     * @return
     */
    Bill updateBill(BillVo billVo);

    /**
     * 新增、修改、删除后分类的余额变化，账户的余额变化
     *
     * @param bill
     */
    void afterUpdateBill(Bill bill);

    /**
     * 查询账单（账本id, 月份, 年份）
     * @param accountBookId 账单id
     * @param year 年
     * @param month 月
     * @return 指定年份，月份，账本下的账单数据
     */
    List<Bill> getBillByAccountBookIdAndYearAndMonth(String accountBookId, String year, String month);
}
