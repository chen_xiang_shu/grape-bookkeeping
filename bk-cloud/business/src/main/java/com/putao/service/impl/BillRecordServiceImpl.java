package com.putao.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.putao.domain.BillRecord;
import com.putao.mapper.BillRecordMapper;
import com.putao.service.BillRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author fuhehuang
 * @email 2628279194@qq.com
 */
@Service
public class BillRecordServiceImpl extends ServiceImpl<BillRecordMapper, BillRecord> implements BillRecordService {

    @Autowired
    private BillRecordMapper billRecordMapper;

    @Override
    public Integer selectDaysByUserId(String userId) {
        return billRecordMapper.selectDaysByUserId(userId);
    }
}
