package com.putao.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

/**
 * Feign拦截器
 */
@Slf4j
public class FeignRequestInterceptor extends CommonFeignInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        super.apply(template);
    }
}
