// const TokenKey = 'bk_phone_jwt';
const colorKey = 'bk_color';
const token = 'bk_token';

// // 认证令牌
// export function getToken() {
// 	return uni.getStorageSync(TokenKey)
// }

// export function setToken(token) {
// 	return uni.setStorageSync(TokenKey, token)
// }

// export function removeToken() {
// 	return uni.removeStorageSync(TokenKey)
// }

// 设置选中的颜色
export function getColor() {
	return uni.getStorageSync(colorKey)
}

export function setColor(color) {
	return uni.setStorageSync(colorKey, color)
}


export function removeColor() {
	return uni.removeStorageSync(colorKey)
}

export function setValue(data) {
	var obj = {
		refresh_token: data.refresh_token,
		jti: data.jti,
		scope: data.scope,
		username: data.username,
		phone: data.phone,
		expires_in: data.expires_in,
		Authorization: data.access_token,
		email: data.email,
		avatar: data.avatar,
		userId: data.id,
	}
	return uni.setStorageSync(token, JSON.stringify(obj));
}

export function getValue() {
	return uni.getStorageSync(token);
}

export function removeValue() {
	return uni.removeStorageSync(token);
}
