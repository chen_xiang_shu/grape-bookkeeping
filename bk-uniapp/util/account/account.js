// 类型（capitalAccount：资金账户、creditAccount：信用账户、investment：投资，rechargeAccount：充值账户）
export const accountArr = [
    {id: 1, name: '资金账户', type: 'capitalAccount', list: [
		{id: 1, name: '银行卡', icon: 'icon-yinhangqia', color: '#c5616a'},
		{id: 2, name: '现金', icon: 'icon-zhanghuyue', color: '#f8d971'},
		{id: 3, name: '支付宝', icon: 'icon-zhifubaozhifu', color: '#4e80ea'},
		{id: 4, name: '微信钱包', icon: 'icon-weixinqianbaocopy', color: '#4aaf36'},
		{id: 5, name: '公积金', icon: 'icon-gongjijinchaxun', color: '#3e9877'},
		{id: 6, name: '社保', icon: 'icon-shebaochaxun', color: '#4f70ee'},
		{id: 7, name: '其他现金账户', icon: 'icon-xianxiazijin', color: '#e1ad51'},
	],},
    {id: 2, name: '信用账户', type: 'creditAccount', list: [
		{id: 1, name: '信用卡', icon: 'icon-24gf-debitCard', color: '#4b7e79'},
		{id: 2, name: '蚂蚁花呗', icon: 'icon-mayihuabei', color: '#578ad4'},
		{id: 3, name: '京东白条', icon: 'icon-jingdongbaitiao', color: '#ae2221'},
		{id: 4, name: '其他信用账户', icon: 'icon-zuanshi', color: '#6d48dd'},
	],},
    {id: 3, name: '投资', type: 'investment', list: [
		{id: 1, name: '股票', icon: 'icon-gupiao-copy', color: '#f0b744'},
		{id: 2, name: '基金', icon: 'icon-jijin', color: '#5e57e2'},
		{id: 3, name: '虚拟货币', icon: 'icon-tubiaozhizuo-02-01', color: '#4e569b'},
		{id: 4, name: '固定资产', icon: 'icon-gudingzichanzhuangushenqingliucheng-03', color: '#419877'},
	],},
    {id: 4, name: '充值账户', type: 'rechargeAccount', list: [
		{id: 1, name: '会员卡', icon: 'icon-huiyuankax', color: '#ddb151'},
		{id: 2, name: '公交卡', icon: 'icon-gongjiaoqiachongzhi', color: '#e16f3f'},
		{id: 3, name: '校园卡', icon: 'icon-iconcopy', color: '#5e59e2'},
		{id: 4, name: '其他充值账户', icon: 'icon-chongzhi', color: '#d15c4a'},
	],},
]
