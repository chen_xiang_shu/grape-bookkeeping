/** 根据字符串切割成一个个字符，在分别统计长度
 * @param {Object} str 字符串
 */
export const checkStrType = (str) => {
	if (!str) {
		return -1;
	}
	if (Number.isFinite(str)) {
		str += "";
	}
	console.log(str);
	var pattern = new RegExp("[\u4E00-\u9FA5]+");
	var length = 0;
	
	// 一个中文字符 32rpx
	// 数字或者英文 16rpx
	str.split("").forEach(item => length += pattern.test(item) ? 28 : 16);
	return length;
	//验证是否是中文
	// if(pattern.test(str)){
	// 	return 0;
	// }
	// //验证是否是英文
	// var pattern2 = new RegExp("[A-Za-z]+");
	// if(pattern2.test(str2)){
	// 	return 1;
	// }
	// //验证是否是数字
	// var pattern3 = new RegExp("[0-9]+");
	// if(pattern3.test(str3)){
	// 	return 2;
	// }
}
/** 根据字符串切割成一个个字符，在分别统计长度
 * @param {Object} str 字符串
 */
export const checkStrType2 = (str, size1, size2) => {
	if (!str) {
		return -1;
	}
	if (Number.isFinite(str)) {
		str += "";
	}
	var pattern = new RegExp("[\u4E00-\u9FA5]+");
	var length = 0;
	
	// 一个中文字符 32rpx
	// 数字或者英文 16rpx
	var arr = str.split("");
	arr.forEach(item => length += pattern.test(item) ? size1 : size2);
	console.log(str + "长度是" + arr.length + " :  ", length);
	return length;
	//验证是否是中文
	// if(pattern.test(str)){
	// 	return 0;
	// }
	// //验证是否是英文
	// var pattern2 = new RegExp("[A-Za-z]+");
	// if(pattern2.test(str2)){
	// 	return 1;
	// }
	// //验证是否是数字
	// var pattern3 = new RegExp("[0-9]+");
	// if(pattern3.test(str3)){
	// 	return 2;
	// }
}

/** 根据字符串切割成一个个字符，在分别统计长度
 * @param {Object} str 字符串
 */
export const calculateLength = (str, size) => {
	if (!str) {
		return -1;
	}
	console.log(str);
	var pattern = new RegExp("[\u4E00-\u9FA5]+");
	var length = 0;
	
	// 一个中文字符 64
	// 数字或者英文 30
	str.split("").forEach(item => length += pattern.test(item) ? 64 : 38);
	return length;
}