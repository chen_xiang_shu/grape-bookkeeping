const successIconUrl = 'https://cdn.uviewui.com/uview/demo/toast/success.png';
const errorIconUrl = 'https://cdn.uviewui.com/uview/demo/toast/error.png';
const loadingIconUrl = 'https://cdn.uviewui.com/uview/demo/toast/loading.png';

export const successTips = (obj, message, callback) => {
	obj.show({message: message, icon: successIconUrl, type: 'success', complete() {
						if (callback) {
							callback();
						}
					}});
}

export const successNoIcon = (obj, message, callback) => {
	obj.show({message: message, icon: 'none', type: 'success', complete() {
						if (callback) {
							callback();
						}
					}});
}

export const errorTips = (obj, message, callback) => {
	obj.show({message: message, icon: errorIconUrl, type: 'error', complete() {
						if (callback) {
							callback();
						}
					}});
}

export const confirm = (tip, content, callback) => {
	uni.showModal({
		title: tip,
		content: content,
		success: function (res) {
			if (res.confirm) {
				if (callback) {
					callback();
				}
				console.log('用户点击确定');
			} else if (res.cancel) {
				console.log('用户点击取消');
			}
		}
	});
}