export const lightColorArr = ['#fffae7',
    '#fdede0',
    '#fef6e3',
    '#fbe6ed',
    '#f8dedd',
    '#f6f4f7',
    '#f1e7f2',
    '#f1fceb',
    '#e6e5ed',
    '#d2eaea',
    '#e3f7ee',
    '#e8f6f7',
    '#e5f3fe',
    '#e0eaf3',
    '#f3f8fe',
    '#d9f6f1',
    '#e1faf4',
    '#e5edfa',
    '#f9e0e6',
    '#f4d7e9',
    '#e6f4dd',
    '#f6d4d3',
    '#f9e5da',
    '#fcf5e5',
    '#efeef6',
    '#e5edfa',
    '#e3f2ed',
    '#fbead0',
    '#f3f3f5',
    '#fafafc',
    '#e7dedf',
    '#eae4e6',
    '#faf6f5',
    '#e0dcdb',
    '#f2f2f0',
    '#ebedea',
    '#e6e8e5',
    '#f4f5f7',
    '#e8e9ed',
    '#ecedf1',
    '#ededed',
    '#e4e4e6',
    '#e2e0eb',
    '#e6e5e1']