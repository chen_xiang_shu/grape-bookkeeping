import {
	getToken,
	getUuid,
	removeToken,
	removeUuid,
	getValue,
	removeValue,
} from '../util/auth.js';

const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';
const FORM = 'FORM';
const DELETE = 'DELETE';
const DOWNLOAD = 'DOWNLOAD';
const Authorization = "Authorization";
const uuid = "uuid";
const env = "PHONE";

// const baseURL = 'http://172.20.10.4:9001';
// const baseURL = 'http://192.168.0.102:9001';

// const systemService = "http://192.168.0.102:10000/system-service";
// const oauthService = "http://192.168.0.102:10000/oauth-service";
// const businessService = "http://192.168.0.102:10000/business-service";

// const systemService = "http://192.168.0.101:10000/system-service";
// const oauthService = "http://192.168.0.101:10000/oauth-service";
// const businessService = "http://192.168.0.101:10000/business-service";

const systemService = "http://192.168.3.58:10000/system-service";
const oauthService = "http://192.168.3.58:10000/oauth-service";
const businessService = "http://192.168.3.58:10000/business-service";
// const baseURL = 'http://192.168.3.163:9001';
// const baseURL = 'http://106.52.152.221:9001';--


function request(method, url, data, serviceName) {
	uni.showLoading({
		title: '加载中',
		mask: true
	});
	var value = getValue();
	let obj;
	if (value) {
		obj = JSON.parse(value);
	}
	//获取当前路由，用于判断是否在个人页面
	let pages = getCurrentPages() //获取加载的页面
	let currentPage = pages[pages.length - 1] //获取当前页面的对象
	let curUlr = currentPage.route //当前页面url
	return new Promise(function(resolve, reject) {
		let header = {}
		if (obj) {
			header = {
				'content-type': 'application/json',
				'Authorization': 'bearer ' + obj.Authorization,
				'refresh_token': obj.refresh_token,
				'jti': obj.jti,
				'scope': obj.scope,
				'username': obj.username,
				'phone': obj.phone,
				'expires_in': obj.expires_in,
				'email': obj.email,
				'avatar': obj.avatar,
				'userId': obj.userId,
				env
			};
		} else {
			header = {
				'content-type': 'application/json',
			};
		}
		var urls;
		if (serviceName == 'systemService') {
			urls = systemService + url;
		} else if (serviceName == 'oauthService') {
			urls = oauthService + url;
		} else if (serviceName == 'businessService') {
			urls = businessService + url;
		}
		uni.request({
			url: urls,
			method: method,
			data: method === POST ? JSON.stringify(data) : data,
			header: header,
			success(res) {
				// 请求成功
				// 判断状态码---errCode状态根据后端定义来判断
				if (res.statusCode == 200) {
					// 判断状态码，如果是登录过期，则移除user对象，跳转到登录页面，让用户重新登录
					uni.hideLoading({});
					// 1、如果是99999,说明是系统异常
					if (res.data.code == 99999) {
						uni.showToast({
							title: '系统异常',
							icon: 'error'
						})
						return;
					} else if (res.data.code == 50002) {
						uni.showToast({
							title: '令牌过期, 请重新登录',
							icon: 'none'
						});
						removeValue();
						setTimeout(() =>
							uni.reLaunch({
								url: '/pages/login/index',
							}), 2000);
						return;
					} else {
						uni.hideLoading({});
						// uni.showToast({
						// 	title: res.data.message,
						// 	icon: 'none'
						// });
					}
					resolve(res.data);
				} else {
					uni.hideLoading({});
					uni.showToast({
						title: '请求返回异常',
					});
					console.log("请求错误" + err);
					//其他异常
					reject('运行时错误,请稍后再试');
				}
			},
			fail(err) {
				uni.hideLoading({});
				uni.showToast({
					title: '请求错误，请稍后再试',
					icon: 'none',
				});
				//请求失败
				reject(err)
			}
		})
	})
}

const api = {
	/****************************************** 接口地址域名 ************************************************/
	basePath: () => {
		return baseURL
	},

	/****************************************** 验证码接口 ************************************************/
	getCode: (data) => request(POST, '/users/sendCode', data, 'systemService'),

	/****************************************** 登录接口 ************************************************/
	// 登录接口
	login: (data) => request(GET, '/oauth/token', data, 'oauthService'),
	// 注册接口
	register: (data) => request(POST, '/users/register', data, 'systemService'),


	/****************************************** 首页接口 ************************************************/

	// 账本集合(包含分类数据)
	getAccountBookList: () => request(GET, '/accountBooks', '', 'businessService'),
	// 新增账本(不包含分类数据，分类数据修改时才存在)
	insertAccountBook: (data) => request(POST, '/accountBooks', data, 'businessService'),
	// 删除账本
	deleteAccountBook: (data) => request(DELETE, '/accountBooks?id=' + data, '', 'businessService'),
	// 修改账本
	updateAccountBook: (data) => request(PUT, '/accountBooks', data, 'businessService'),
	// 新增分类
	insertCategory: (data) => request(POST, '/categorys', data, 'businessService'),
	// 删除分类
	deleteCategory: (data) => request(DELETE, '/categorys?id=' + data, '', 'businessService'),
	// 修改分类
	updateCategory: (data) => request(PUT, '/categorys', data, 'businessService'),




	/****************************************** 账户接口 ************************************************/

	// 账户集合
	getAccountList: () => request(GET, '/accounts', '', 'businessService'),
	// 新增账户
	insertAccount: (data) => request(POST, '/accounts', data, 'businessService'),
	// 删除账户
	deleteAccount: (data) => request(DELETE, '/accounts?id=' + data, '', 'businessService'),
	// 修改账户
	updateAccount: (data) => request(PUT, '/accounts', data, 'businessService'),

	/****************************************** 明细相关 ************************************************/
	getData: (data) => request(GET, '/bills?' + data, '', 'businessService'),
	// 修改
	updateBill: (data) => request(PUT, '/bills', data, 'businessService'),
	// 新增
	insertBill: (data) => request(POST, '/bills', data, 'businessService'),
	// 删除
	deleteBill: (id) => request(DELETE, '/bills?id=' + id, '', 'businessService'),

	/****************************************** 报表相关 ************************************************/
	getCharts: (data) => request(GET, '/charts?' + data, '', 'businessService'),

	/****************************************** 设置相关 ************************************************/
	// 扫码成功后发送请求给后端
	scanSuccess: (data)  => request(GET, '/users/scanSuccess?' + data, '', 'systemService'),
	// 取消登录
	cancelLoginPc: (data)  => request(GET, '/users/cancelLoginPc?key=' + data, '', 'systemService'),
	// 确认登录pc端 http://localhost:10000/oauth-service/oauth/token?grant_type=pas&key=65680076-d27a-48c7-83be-1ed9d73cae7f&userId=1&client_id=qrcode_client_id&client_secret=123
	loginPc: (data) => request(GET, '/oauth/token?grant_type=qrcode&client_id=qrcode_client_id&client_secret=123&' + data, '', 'oauthService'),
	// 查询总天数
	getDay: (data) => request(GET, '/billRecords/getDay?' + data, '', 'businessService'),

}
module.exports = {
	api: api
}
