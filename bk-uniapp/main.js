import App from './App'



// #ifndef VUE3
import Vue from 'vue'
// main.js
import uView from '@/uni_modules/uview-ui';
import lodash from 'lodash';
Vue.use(uView)

import {msg, redirectTo, switchTab, sleep, navigateTo} from '@/util/util.js'
Vue.prototype.msg = msg;
Vue.prototype.redirectTo = redirectTo;
Vue.prototype.switchTab = switchTab;
Vue.prototype.navigateTo = navigateTo;
Vue.prototype.sleep = sleep;
var _ = require('lodash');
Vue.prototype._ = lodash;

import {api} from 'api/api.js';
Vue.prototype.$req = api;


// 上传图片路径
Vue.prototype.uploadUrl = "http://192.168.0.101:9001/test/upload";


// 全局颜色使用
import {deepColorArr, lightColorArr, circularColorArr, iconColorArr, accountDetailCardBgcColorArr} from '@/util/color/color.js';
Vue.prototype.deepColorArr = deepColorArr;
Vue.prototype.lightColorArr = lightColorArr;
Vue.prototype.circularColorArr = circularColorArr;
Vue.prototype.iconColorArr = iconColorArr;

// 单个使用 深色
Vue.prototype.themeYellowColor = '#f4de6b';								// 主题黄色
Vue.prototype.themePurpleColor = '#4d4f91';								// 主题紫色

// 字体颜色
Vue.prototype.$themeGreenColor = '#41d087';								// 绿色
Vue.prototype.$themeRedColor = '#cf6d6b';								// 红色

// 设置页面中的账本管理卡片的两种背景色
Vue.prototype.$settingAccountBookBgcRedColor = '#efd8e8';				// 红色
Vue.prototype.$settingAccountBookBgcYellowColor = '#fdfae9';			// 绿色
Vue.prototype.$deepBgcRedColor = '#d64193';				// 红色
Vue.prototype.$deepBgcYellowColor = '#fae590';			// 绿色
// 设置页面中的账本管理卡片的两种背景色
Vue.prototype.$settingAccountBookFontRedColor = '#bf5492';				// 红色
Vue.prototype.$settingAccountBookFontYellowColor = '#f4e7a1';			// 绿色

// 时间函数
import dayjs from 'dayjs';
Vue.prototype.$dayjs = dayjs;



Vue.prototype.time = 0;
Vue.prototype.page = '';

// 全局使用图标
import {dailyIconArr, restaurantIconArr, trafficIconArr, travelIconArr, familyIconArr, shoppingIconArr, otherIconArr, iconCategoryList} from '@/util/icon/icon.js'
Vue.prototype.dailyIconArr = dailyIconArr;
Vue.prototype.restaurantIconArr = restaurantIconArr;
Vue.prototype.trafficIconArr = trafficIconArr;
Vue.prototype.travelIconArr = travelIconArr;
Vue.prototype.familyIconArr = familyIconArr;
Vue.prototype.shoppingIconArr = shoppingIconArr;
Vue.prototype.otherIconArr = otherIconArr;
Vue.prototype.iconCategoryList = iconCategoryList;

// 全局使用账户图标集合
import {accountArr} from '@/util/account/account.js';
Vue.prototype.accountArr = accountArr;

// 全局使用银行卡图标集合
import {bankIconArr} from '@/util/bank/bank.js';
Vue.prototype.bankIconArr = bankIconArr;

// 不显示金额
Vue.prototype.secret = '¥****';

// 账户卡片背景色
Vue.prototype.accountDetailCardBgcColorArr = accountDetailCardBgcColorArr;


// 封装的消息提示的方法
uni.$showMsg = function(title = "数据加载失败！", duration = 1500) {
	uni.showToast({
		title,
		duration,
	})
}
// 如果不传参，title和duration就显示为默认值 


// uview 的提示
import {successTips, errorTips, confirm, successNoIcon} from '@/util/tips/tips.js';
Vue.prototype.successTips = successTips;
Vue.prototype.errorTips = errorTips;
Vue.prototype.confirm = confirm;
Vue.prototype.successNoIcon = successNoIcon;





Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import { msg } from './util/util';
import {circularColorArr} from "./util/circularColor";
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif